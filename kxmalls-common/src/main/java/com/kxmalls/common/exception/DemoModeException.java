package com.kxmalls.common.exception;

/**
 * 演示模式异常
 *
 * @author kxmalls
 */
public class DemoModeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DemoModeException() {
    }
}
