package com.kxmalls.templates.mapper;

import com.kxmalls.templates.domain.WmTemplatesRegion;
import com.kxmalls.templates.domain.vo.WmTemplatesRegionVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 运费模板区域Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface WmTemplatesRegionMapper extends BaseMapperPlus<WmTemplatesRegionMapper, WmTemplatesRegion, WmTemplatesRegionVo> {

}
