package com.kxmalls.templates.mapper;

import com.kxmalls.templates.domain.WmTemplatesItem;
import com.kxmalls.templates.domain.vo.WmTemplatesItemVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 运费模板详情Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface WmTemplatesItemMapper extends BaseMapperPlus<WmTemplatesItemMapper, WmTemplatesItem, WmTemplatesItemVo> {

}
