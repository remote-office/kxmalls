package com.kxmalls.templates.mapper;

import com.kxmalls.templates.domain.WmTemplates;
import com.kxmalls.templates.domain.vo.WmTemplatesVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 运费模板Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface WmTemplatesMapper extends BaseMapperPlus<WmTemplatesMapper, WmTemplates, WmTemplatesVo> {

}
