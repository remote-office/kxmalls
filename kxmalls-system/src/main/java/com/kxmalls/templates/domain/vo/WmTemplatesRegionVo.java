package com.kxmalls.templates.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 运费模板区域视图对象 wm_templates_region
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@Data
@ExcelIgnoreUnannotated
public class WmTemplatesRegionVo {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ExcelProperty(value = "编号")
    private Long id;

    /**
     * 省ID
     */
    @ExcelProperty(value = "省ID")
    private Long provinceId;

    /**
     * 模板ID
     */
    @ExcelProperty(value = "模板ID")
    private Long tempId;

    /**
     * 城市ID
     */
    @ExcelProperty(value = "城市ID")
    private Long cityId;

    /**
     * 首件
     */
    @ExcelProperty(value = "首件")
    private BigDecimal first;

    /**
     * 首件运费
     */
    @ExcelProperty(value = "首件运费")
    private BigDecimal firstPrice;

    /**
     * 续件
     */
    @ExcelProperty(value = "续件")
    private BigDecimal continues;

    /**
     * 续件运费
     */
    @ExcelProperty(value = "续件运费")
    private BigDecimal continuePrice;

    /**
     * 计费方式
     */
    @ExcelProperty(value = "计费方式")
    private Integer type;

    /**
     * 分组唯一值
     */
    @ExcelProperty(value = "分组唯一值")
    private String uniqid;


}
