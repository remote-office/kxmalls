package com.kxmalls.templates.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.kxmalls.common.core.domain.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 运费模板区域对象 wm_templates_region
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@TableName("wm_templates_region")
public class WmTemplatesRegion extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 省ID
     */
    private Long provinceId;
    /**
     * 模板ID
     */
    private Long tempId;
    /**
     * 城市ID
     */
    private Long cityId;
    /**
     * 首件
     */
    private BigDecimal first;
    /**
     * 首件运费
     */
    private BigDecimal firstPrice;
    /**
     * 续件
     */
    private BigDecimal continues;
    /**
     * 续件运费
     */
    private BigDecimal continuePrice;
    /**
     * 计费方式
     */
    private Integer type;
    /**
     * 分组唯一值
     */
    private String uniqid;

}
