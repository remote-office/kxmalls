package com.kxmalls.templates.domain.bo;

import com.kxmalls.common.core.domain.BaseEntity;
import com.kxmalls.system.domain.bo.SysRegionInfoBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * 运费模板业务对象 wm_templates
 *
 * @author kxmalls
 * @date 2023-02-08
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WmTemplatesBo extends BaseEntity {

    /**
     * 模板ID
     */
    private Long id;

    /**
     * 模板名称
     */
    private String name;

    /**
     * 计费方式
     */
    private Integer type;

    /**
     * 指定包邮开关
     */
    private Integer appoint;

    /**
     *
     */
    private Integer isDel;

    /**
     * 排序
     */
    private Long sort;

    /** 地域以及费用 */
    @NotNull(message = "请设置地域")
    private List<SysRegionInfoBo> regionInfo;


    /** 指定包邮内容 */
    private List<AppointInfoBo> appointInfo;

}
