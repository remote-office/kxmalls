package com.kxmalls.templates.domain.bo;

import com.kxmalls.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 运费模板详情业务对象 wm_templates_item
 *
 * @author kxmalls
 * @date 2023-02-08
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WmTemplatesItemBo extends BaseEntity {

    /**
     * 编号
     */
    private Long id;

    /**
     * 省ID
     */
    private Long provinceId;

    /**
     * 模板ID
     */
    private Long tempId;

    /**
     * 城市ID
     */
    private Long cityId;

    /**
     * 包邮件数
     */
    private BigDecimal number;

    /**
     * 包邮金额
     */
    private BigDecimal price;

    /**
     * 计费方式
     */
    private Integer type;

    /**
     * 分组唯一值
     */
    private String uniqid;


}
