package com.kxmalls.templates.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 运费模板详情视图对象 wm_templates_item
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@Data
@ExcelIgnoreUnannotated
public class WmTemplatesItemVo {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ExcelProperty(value = "编号")
    private Long id;

    /**
     * 省ID
     */
    @ExcelProperty(value = "省ID")
    private Long provinceId;

    /**
     * 模板ID
     */
    @ExcelProperty(value = "模板ID")
    private Long tempId;

    /**
     * 城市ID
     */
    @ExcelProperty(value = "城市ID")
    private Long cityId;

    /**
     * 包邮件数
     */
    @ExcelProperty(value = "包邮件数")
    private BigDecimal number;

    /**
     * 包邮金额
     */
    @ExcelProperty(value = "包邮金额")
    private BigDecimal price;

    /**
     * 计费方式
     */
    @ExcelProperty(value = "计费方式")
    private Integer type;

    /**
     * 分组唯一值
     */
    @ExcelProperty(value = "分组唯一值")
    private String uniqid;


}
