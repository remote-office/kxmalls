package com.kxmalls.templates.domain.bo;

import com.kxmalls.system.domain.bo.SysRegionBo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class AppointInfoBo {

    /** 包邮件数 */
    private String a_num;

    /** 包邮费用 */
    private String a_price;

    /** 包邮地区 */
    private List<SysRegionBo> place;

    private String placeName;

}
