package com.kxmalls.templates.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.kxmalls.templates.domain.bo.AppointInfoBo;
import com.kxmalls.common.core.domain.BaseEntity;
import com.kxmalls.system.domain.bo.SysRegionInfoBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


/**
 * 运费模板对象 wm_templates
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "wm_templates", autoResultMap = true)
public class WmTemplates extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 模板ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 模板名称
     */
    private String name;
    /**
     * 计费方式
     */
    private Integer type;
    /**
     * 地域以及费用
     */
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private List<SysRegionInfoBo> regionInfo;
    /**
     * 指定包邮开关
     */
    private Integer appoint;
    /**
     * 指定包邮内容
     */
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private List<AppointInfoBo> appointInfo;
    /**
     *
     */
    private Integer isDel;
    /**
     * 排序
     */
    private Long sort;

}
