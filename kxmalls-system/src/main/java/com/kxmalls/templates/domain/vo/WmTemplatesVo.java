package com.kxmalls.templates.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.kxmalls.system.domain.bo.SysRegionInfoBo;
import com.kxmalls.templates.domain.bo.AppointInfoBo;
import lombok.Data;

import java.util.List;


/**
 * 运费模板视图对象 wm_templates
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@Data
@ExcelIgnoreUnannotated
@TableName(autoResultMap = true)
public class WmTemplatesVo {

    private static final long serialVersionUID = 1L;

    /**
     * 模板ID
     */
    @ExcelProperty(value = "模板ID")
    private Long id;

    /**
     * 模板名称
     */
    @ExcelProperty(value = "模板名称")
    private String name;

    /**
     * 计费方式
     */
    @ExcelProperty(value = "计费方式")
    private Integer type;

    /**
     * 地域以及费用
     */
    @ExcelProperty(value = "地域以及费用")
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private List<SysRegionInfoBo> regionInfo;

    /**
     * 指定包邮开关
     */
    @ExcelProperty(value = "指定包邮开关")
    private Integer appoint;

    /**
     * 指定包邮内容
     */
    @ExcelProperty(value = "指定包邮内容")
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private List<AppointInfoBo> appointInfo;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Integer isDel;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Long sort;


}
