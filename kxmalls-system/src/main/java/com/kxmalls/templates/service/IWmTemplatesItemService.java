package com.kxmalls.templates.service;

import com.kxmalls.templates.domain.vo.WmTemplatesItemVo;
import com.kxmalls.templates.domain.bo.WmTemplatesItemBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 运费模板详情Service接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface IWmTemplatesItemService {

    /**
     * 查询运费模板详情
     */
    WmTemplatesItemVo queryById(Long id);

    /**
     * 查询运费模板详情列表
     */
    TableDataInfo<WmTemplatesItemVo> queryPageList(WmTemplatesItemBo bo, PageQuery pageQuery);

    /**
     * 查询运费模板详情列表
     */
    List<WmTemplatesItemVo> queryList(WmTemplatesItemBo bo);

    /**
     * 新增运费模板详情
     */
    Boolean insertByBo(WmTemplatesItemBo bo);

    /**
     * 修改运费模板详情
     */
    Boolean updateByBo(WmTemplatesItemBo bo);

    /**
     * 校验并批量删除运费模板详情信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
