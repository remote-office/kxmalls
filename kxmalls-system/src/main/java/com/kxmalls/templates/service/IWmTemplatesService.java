package com.kxmalls.templates.service;

import com.kxmalls.templates.domain.vo.WmTemplatesVo;
import com.kxmalls.templates.domain.bo.WmTemplatesBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 运费模板Service接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface IWmTemplatesService {

    /**
     * 查询运费模板
     */
    WmTemplatesVo queryById(Long id);

    /**
     * 查询运费模板列表
     */
    TableDataInfo<WmTemplatesVo> queryPageList(WmTemplatesBo bo, PageQuery pageQuery);

    /**
     * 查询运费模板列表
     */
    List<WmTemplatesVo> queryList(WmTemplatesBo bo);

    /**
     * 新增运费模板
     */
    Boolean insertAndupdateByBo(WmTemplatesBo bo);

    /**
     * 修改运费模板
     */
    Boolean updateByBo(WmTemplatesBo bo);

    /**
     * 校验并批量删除运费模板信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
