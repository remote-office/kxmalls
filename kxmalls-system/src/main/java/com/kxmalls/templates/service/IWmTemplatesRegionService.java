package com.kxmalls.templates.service;

import com.kxmalls.templates.domain.vo.WmTemplatesRegionVo;
import com.kxmalls.templates.domain.bo.WmTemplatesRegionBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 运费模板区域Service接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface IWmTemplatesRegionService {

    /**
     * 查询运费模板区域
     */
    WmTemplatesRegionVo queryById(Long id);

    /**
     * 查询运费模板区域列表
     */
    TableDataInfo<WmTemplatesRegionVo> queryPageList(WmTemplatesRegionBo bo, PageQuery pageQuery);

    /**
     * 查询运费模板区域列表
     */
    List<WmTemplatesRegionVo> queryList(WmTemplatesRegionBo bo);

    /**
     * 新增运费模板区域
     */
    Boolean insertByBo(WmTemplatesRegionBo bo);

    /**
     * 修改运费模板区域
     */
    Boolean updateByBo(WmTemplatesRegionBo bo);

    /**
     * 校验并批量删除运费模板区域信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
