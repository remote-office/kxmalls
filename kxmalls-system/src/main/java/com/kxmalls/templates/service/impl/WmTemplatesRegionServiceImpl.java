package com.kxmalls.templates.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.templates.domain.bo.WmTemplatesRegionBo;
import com.kxmalls.templates.domain.vo.WmTemplatesRegionVo;
import com.kxmalls.templates.domain.WmTemplatesRegion;
import com.kxmalls.templates.mapper.WmTemplatesRegionMapper;
import com.kxmalls.templates.service.IWmTemplatesRegionService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 运费模板区域Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@RequiredArgsConstructor
@Service
public class WmTemplatesRegionServiceImpl implements IWmTemplatesRegionService {

    private final WmTemplatesRegionMapper baseMapper;

    /**
     * 查询运费模板区域
     */
    @Override
    public WmTemplatesRegionVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询运费模板区域列表
     */
    @Override
    public TableDataInfo<WmTemplatesRegionVo> queryPageList(WmTemplatesRegionBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmTemplatesRegion> lqw = buildQueryWrapper(bo);
        Page<WmTemplatesRegionVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询运费模板区域列表
     */
    @Override
    public List<WmTemplatesRegionVo> queryList(WmTemplatesRegionBo bo) {
        LambdaQueryWrapper<WmTemplatesRegion> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmTemplatesRegion> buildQueryWrapper(WmTemplatesRegionBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmTemplatesRegion> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getProvinceId() != null, WmTemplatesRegion::getProvinceId, bo.getProvinceId());
        lqw.eq(bo.getTempId() != null, WmTemplatesRegion::getTempId, bo.getTempId());
        lqw.eq(bo.getCityId() != null, WmTemplatesRegion::getCityId, bo.getCityId());
        lqw.eq(bo.getFirst() != null, WmTemplatesRegion::getFirst, bo.getFirst());
        lqw.eq(bo.getFirstPrice() != null, WmTemplatesRegion::getFirstPrice, bo.getFirstPrice());
        lqw.eq(bo.getContinues() != null, WmTemplatesRegion::getContinues, bo.getContinues());
        lqw.eq(bo.getContinuePrice() != null, WmTemplatesRegion::getContinuePrice, bo.getContinuePrice());
        lqw.eq(bo.getType() != null, WmTemplatesRegion::getType, bo.getType());
        lqw.eq(StringUtils.isNotBlank(bo.getUniqid()), WmTemplatesRegion::getUniqid, bo.getUniqid());
        return lqw;
    }

    /**
     * 新增运费模板区域
     */
    @Override
    public Boolean insertByBo(WmTemplatesRegionBo bo) {
        WmTemplatesRegion add = BeanUtil.toBean(bo, WmTemplatesRegion.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改运费模板区域
     */
    @Override
    public Boolean updateByBo(WmTemplatesRegionBo bo) {
        WmTemplatesRegion update = BeanUtil.toBean(bo, WmTemplatesRegion.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmTemplatesRegion entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除运费模板区域
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
