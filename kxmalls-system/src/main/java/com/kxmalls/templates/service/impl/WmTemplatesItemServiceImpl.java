package com.kxmalls.templates.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.templates.domain.bo.WmTemplatesItemBo;
import com.kxmalls.templates.domain.vo.WmTemplatesItemVo;
import com.kxmalls.templates.domain.WmTemplatesItem;
import com.kxmalls.templates.mapper.WmTemplatesItemMapper;
import com.kxmalls.templates.service.IWmTemplatesItemService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 运费模板详情Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@RequiredArgsConstructor
@Service
public class WmTemplatesItemServiceImpl implements IWmTemplatesItemService {

    private final WmTemplatesItemMapper baseMapper;

    /**
     * 查询运费模板详情
     */
    @Override
    public WmTemplatesItemVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询运费模板详情列表
     */
    @Override
    public TableDataInfo<WmTemplatesItemVo> queryPageList(WmTemplatesItemBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmTemplatesItem> lqw = buildQueryWrapper(bo);
        Page<WmTemplatesItemVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询运费模板详情列表
     */
    @Override
    public List<WmTemplatesItemVo> queryList(WmTemplatesItemBo bo) {
        LambdaQueryWrapper<WmTemplatesItem> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmTemplatesItem> buildQueryWrapper(WmTemplatesItemBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmTemplatesItem> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getProvinceId() != null, WmTemplatesItem::getProvinceId, bo.getProvinceId());
        lqw.eq(bo.getTempId() != null, WmTemplatesItem::getTempId, bo.getTempId());
        lqw.eq(bo.getCityId() != null, WmTemplatesItem::getCityId, bo.getCityId());
        lqw.eq(bo.getNumber() != null, WmTemplatesItem::getNumber, bo.getNumber());
        lqw.eq(bo.getPrice() != null, WmTemplatesItem::getPrice, bo.getPrice());
        lqw.eq(bo.getType() != null, WmTemplatesItem::getType, bo.getType());
        lqw.eq(StringUtils.isNotBlank(bo.getUniqid()), WmTemplatesItem::getUniqid, bo.getUniqid());
        return lqw;
    }

    /**
     * 新增运费模板详情
     */
    @Override
    public Boolean insertByBo(WmTemplatesItemBo bo) {
        WmTemplatesItem add = BeanUtil.toBean(bo, WmTemplatesItem.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改运费模板详情
     */
    @Override
    public Boolean updateByBo(WmTemplatesItemBo bo) {
        WmTemplatesItem update = BeanUtil.toBean(bo, WmTemplatesItem.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmTemplatesItem entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除运费模板详情
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
