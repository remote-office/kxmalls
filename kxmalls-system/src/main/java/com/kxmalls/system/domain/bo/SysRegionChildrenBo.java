package com.kxmalls.system.domain.bo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRegionChildrenBo {

    private String cityId;
}
