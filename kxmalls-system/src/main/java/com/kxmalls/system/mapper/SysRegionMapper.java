package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysRegion;
import com.kxmalls.system.domain.vo.SysRegionVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 中国地区系统Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface SysRegionMapper extends BaseMapperPlus<SysRegionMapper, SysRegion, SysRegionVo> {

}
