package com.kxmalls.system.mapper;

import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.system.domain.SysUserPost;

/**
 * 用户与岗位关联表 数据层
 *
 * @author Lion Li
 */
public interface SysUserPostMapper extends BaseMapperPlus<SysUserPostMapper, SysUserPost, SysUserPost> {

}
