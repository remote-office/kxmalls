package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysOss;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.system.domain.vo.SysOssVo;

/**
 * 文件上传 数据层
 *
 * @author Lion Li
 */
public interface SysOssMapper extends BaseMapperPlus<SysOssMapper, SysOss, SysOssVo> {
}
