package com.kxmalls.system.mapper;

import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.system.domain.SysConfig;

/**
 * 参数配置 数据层
 *
 * @author Lion Li
 */
public interface SysConfigMapper extends BaseMapperPlus<SysConfigMapper, SysConfig, SysConfig> {

}
