package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysOssConfig;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.system.domain.vo.SysOssConfigVo;

/**
 * 对象存储配置Mapper接口
 *
 * @author Lion Li
 * @author 孤舟烟雨
 * @date 2021-08-13
 */
public interface SysOssConfigMapper extends BaseMapperPlus<SysOssConfigMapper, SysOssConfig, SysOssConfigVo> {

}
