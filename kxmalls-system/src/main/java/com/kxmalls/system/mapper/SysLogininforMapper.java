package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysLogininfor;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author Lion Li
 */
public interface SysLogininforMapper extends BaseMapperPlus<SysLogininforMapper, SysLogininfor, SysLogininfor> {

}
