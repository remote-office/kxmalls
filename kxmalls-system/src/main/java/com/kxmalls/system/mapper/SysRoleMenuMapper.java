package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysRoleMenu;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 角色与菜单关联表 数据层
 *
 * @author Lion Li
 */
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenuMapper, SysRoleMenu, SysRoleMenu> {

}
