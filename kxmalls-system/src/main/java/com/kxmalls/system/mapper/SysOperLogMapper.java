package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysOperLog;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 操作日志 数据层
 *
 * @author Lion Li
 */
public interface SysOperLogMapper extends BaseMapperPlus<SysOperLogMapper, SysOperLog, SysOperLog> {

}
