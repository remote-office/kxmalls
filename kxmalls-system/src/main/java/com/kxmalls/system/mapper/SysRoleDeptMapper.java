package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysRoleDept;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 角色与部门关联表 数据层
 *
 * @author Lion Li
 */
public interface SysRoleDeptMapper extends BaseMapperPlus<SysRoleDeptMapper, SysRoleDept, SysRoleDept> {

}
