package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysExpress;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.system.domain.vo.SysExpressVo;

/**
 * 快递公司Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface SysExpressMapper extends BaseMapperPlus<SysExpressMapper, SysExpress, SysExpressVo> {

}
