package com.kxmalls.system.mapper;

import com.kxmalls.system.domain.SysNotice;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 通知公告表 数据层
 *
 * @author Lion Li
 */
public interface SysNoticeMapper extends BaseMapperPlus<SysNoticeMapper, SysNotice, SysNotice> {

}
