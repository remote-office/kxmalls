package com.kxmalls.user.mapper;

import com.kxmalls.user.domain.WmUserLevel;
import com.kxmalls.user.domain.vo.WmUserLevelVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 用户等级Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-14
 */
public interface WmUserLevelMapper extends BaseMapperPlus<WmUserLevelMapper, WmUserLevel, WmUserLevelVo> {

}
