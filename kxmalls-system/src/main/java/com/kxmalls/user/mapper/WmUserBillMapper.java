package com.kxmalls.user.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kxmalls.user.domain.WmUserBill;
import com.kxmalls.user.domain.vo.WmUserBillVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

/**
 * 用户账单Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-14
 */
public interface WmUserBillMapper extends BaseMapperPlus<WmUserBillMapper, WmUserBill, WmUserBillVo> {


    Page<WmUserBillVo> selectVoPageList(@Param("page") Page<Object> build,@Param(Constants.WRAPPER) QueryWrapper<WmUserBill> lqw);
}
