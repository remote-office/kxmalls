package com.kxmalls.user.mapper;

import com.kxmalls.user.domain.WmUserLevelSetting;
import com.kxmalls.user.domain.vo.WmUserLevelSettingVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 设置用户等级Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-21
 */
public interface WmUserLevelSettingMapper extends BaseMapperPlus<WmUserLevelSettingMapper, WmUserLevelSetting, WmUserLevelSettingVo> {

}
