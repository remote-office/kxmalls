package com.kxmalls.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kxmalls.user.domain.WmUser;
import com.kxmalls.user.domain.vo.WmPromUserVo;
import com.kxmalls.user.domain.vo.WmUserVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-14
 */
public interface WmUserMapper extends BaseMapperPlus<WmUserMapper, WmUser, WmUserVo> {


    @Select("<script>SELECT u.uid,u.nickname,u.avatar,DATE_FORMAT(u.create_time,'%Y/%m/%d') as time," +
        "u.spread_count as childCount,COUNT(o.id) as orderCount," +
        "IFNULL(SUM(o.pay_price),0) as numberCount FROM yx_user u " +
        "LEFT JOIN yx_store_order o ON u.uid=o.uid " +
        "WHERE u.uid in <foreach item='id' index='index' collection='uids' " +
        " open='(' separator=',' close=')'>" +
        "   #{id}" +
        " </foreach> <if test='keyword != null'>" +
        " AND ( u.nickname LIKE CONCAT(CONCAT('%',#{keyword}),'%') OR u.phone LIKE CONCAT(CONCAT('%',#{keyword}),'%'))</if>" +
        " GROUP BY u.uid ORDER BY #{orderByStr} " +
        "</script>")
    List<WmPromUserVo> getUserSpreadCountList(Page page,
                                             @Param("uids") List uids,
                                             @Param("keyword") String keyword,
                                             @Param("orderByStr") String orderBy);
}
