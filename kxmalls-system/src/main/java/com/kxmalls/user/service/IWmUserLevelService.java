package com.kxmalls.user.service;

import com.kxmalls.user.domain.vo.WmUserLevelVo;
import com.kxmalls.user.domain.bo.WmUserLevelBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 用户等级Service接口
 *
 * @author kxmalls
 * @date 2023-02-14
 */
public interface IWmUserLevelService {

    /**
     * 查询用户等级
     */
    WmUserLevelVo queryById(Long id);

    /**
     * 查询用户等级列表
     */
    TableDataInfo<WmUserLevelVo> queryPageList(WmUserLevelBo bo, PageQuery pageQuery);

    /**
     * 查询用户等级列表
     */
    List<WmUserLevelVo> queryList(WmUserLevelBo bo);

    /**
     * 新增用户等级
     */
    Boolean insertByBo(WmUserLevelBo bo);

    /**
     * 修改用户等级
     */
    Boolean updateByBo(WmUserLevelBo bo);

    /**
     * 校验并批量删除用户等级信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
