package com.kxmalls.user.service;

import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.user.domain.bo.WmUserBo;
import com.kxmalls.user.domain.vo.WmPromUserVo;
import com.kxmalls.user.domain.vo.WmUserVo;

import java.util.Collection;
import java.util.List;

/**
 * 用户Service接口
 *
 * @author kxmalls
 * @date 2023-02-14
 */
public interface IWmUserService {

    /**
     * 查询用户
     */
    WmUserVo queryById(Long uid);

    /**
     * 查询用户列表
     */
    TableDataInfo<WmUserVo> queryPageList(WmUserBo bo, PageQuery pageQuery);

    /**
     * 查询用户列表
     */
    List<WmUserVo> queryList(WmUserBo bo);

    /**
     * 新增用户
     */
    Boolean insertByBo(WmUserBo bo);

    /**
     * 修改用户
     */
    Boolean updateByBo(WmUserBo bo);

    /**
     * 校验并批量删除用户信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 查看下级
     *
     * @return
     */
    List<WmPromUserVo> querySpread(WmUserBo bo);


    /**
     * 获取我的分销下人员列表
     *
     * @param uid     uid
     * @param page    page
     * @param limit   limit
     * @param grade   ShopCommonEnum.GRADE_0
     * @param keyword 关键字搜索
     * @param sort    排序
     * @return list
     */
    List<WmPromUserVo> getUserSpreadGrade(Long uid, int page, int limit, Integer grade, String keyword, String sort);

    /**
     * 更改状态
     */
    void onStatus(Long id, Integer status);

    /**
     * 修改余额
     */
    void updateMoney(WmUserBo param);

    /**
     * 通过uid获取用户信息
     *
     * @param uid
     * @return
     */
    WmUserVo selectByUid(Long uid);
}
