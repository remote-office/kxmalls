package com.kxmalls.user.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kxmalls.user.domain.WmUser;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.enums.BillDetailEnum;
import com.kxmalls.common.enums.ShopCommonEnum;
import com.kxmalls.common.utils.StringUtils;
import com.kxmalls.user.domain.bo.WmUserBo;
import com.kxmalls.user.domain.vo.WmPromUserVo;
import com.kxmalls.user.domain.vo.WmUserVo;
import com.kxmalls.user.mapper.WmUserMapper;
import com.kxmalls.user.service.IWmUserBillService;
import com.kxmalls.user.service.IWmUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-14
 */
@RequiredArgsConstructor
@Service
public class WmUserServiceImpl implements IWmUserService {

    private final WmUserMapper baseMapper;

    private final IWmUserBillService userBillService;

    /**
     * 查询用户
     */
    @Override
    public WmUserVo queryById(Long uid) {
        return baseMapper.selectVoById(uid);
    }

    /**
     * 查询用户列表
     */
    @Override
    public TableDataInfo<WmUserVo> queryPageList(WmUserBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmUser> lqw = buildQueryWrapper(bo);
        Page<WmUserVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询用户列表
     */
    @Override
    public List<WmUserVo> queryList(WmUserBo bo) {
        LambdaQueryWrapper<WmUser> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmUser> buildQueryWrapper(WmUserBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmUser> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getUsername()), WmUser::getUsername, bo.getUsername());
        lqw.eq(StringUtils.isNotBlank(bo.getPassword()), WmUser::getPassword, bo.getPassword());
        lqw.like(StringUtils.isNotBlank(bo.getRealName()), WmUser::getRealName, bo.getRealName());
        lqw.eq(bo.getBirthday() != null, WmUser::getBirthday, bo.getBirthday());
        lqw.eq(StringUtils.isNotBlank(bo.getCardId()), WmUser::getCardId, bo.getCardId());
        lqw.eq(StringUtils.isNotBlank(bo.getMark()), WmUser::getMark, bo.getMark());
        lqw.eq(bo.getPartnerId() != null, WmUser::getPartnerId, bo.getPartnerId());
        lqw.eq(bo.getGroupId() != null, WmUser::getGroupId, bo.getGroupId());
        lqw.like(StringUtils.isNotBlank(bo.getNickname()), WmUser::getNickname, bo.getNickname());
        lqw.eq(StringUtils.isNotBlank(bo.getAvatar()), WmUser::getAvatar, bo.getAvatar());
        lqw.likeRight(StringUtils.isNotBlank(bo.getPhone()), WmUser::getPhone, bo.getPhone());
        lqw.eq(StringUtils.isNotBlank(bo.getAddIp()), WmUser::getAddIp, bo.getAddIp());
        lqw.eq(StringUtils.isNotBlank(bo.getLastIp()), WmUser::getLastIp, bo.getLastIp());
        lqw.eq(bo.getNowMoney() != null, WmUser::getNowMoney, bo.getNowMoney());
        lqw.eq(bo.getBrokeragePrice() != null, WmUser::getBrokeragePrice, bo.getBrokeragePrice());
        lqw.eq(bo.getIntegral() != null, WmUser::getIntegral, bo.getIntegral());
        lqw.eq(bo.getSignNum() != null, WmUser::getSignNum, bo.getSignNum());
        lqw.eq(bo.getStatus() != null, WmUser::getStatus, bo.getStatus());
        lqw.eq(bo.getLevel() != null, WmUser::getLevel, bo.getLevel());
        lqw.eq(bo.getSpreadUid() != null, WmUser::getSpreadUid, bo.getSpreadUid());
        lqw.eq(bo.getSpreadTime() != null, WmUser::getSpreadTime, bo.getSpreadTime());
        lqw.eq(StringUtils.isNotBlank(bo.getUserType()), WmUser::getUserType, bo.getUserType());
        lqw.eq(bo.getIsPromoter() != null, WmUser::getIsPromoter, bo.getIsPromoter());
        lqw.eq(bo.getPayCount() != null, WmUser::getPayCount, bo.getPayCount());
        lqw.eq(bo.getSpreadCount() != null, WmUser::getSpreadCount, bo.getSpreadCount());
        lqw.eq(StringUtils.isNotBlank(bo.getAddres()), WmUser::getAddres, bo.getAddres());
        lqw.eq(bo.getAdminid() != null, WmUser::getAdminid, bo.getAdminid());
        lqw.eq(StringUtils.isNotBlank(bo.getLoginType()), WmUser::getLoginType, bo.getLoginType());
        lqw.eq(StringUtils.isNotBlank(bo.getWxProfile()), WmUser::getWxProfile, bo.getWxProfile());
        lqw.eq(bo.getIsDel() != null, WmUser::getIsDel, bo.getIsDel());
        return lqw;
    }

    /**
     * 新增用户
     */
    @Override
    public Boolean insertByBo(WmUserBo bo) {
        WmUser add = BeanUtil.toBean(bo, WmUser.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setUid(add.getUid());
        }
        return flag;
    }

    /**
     * 修改用户
     */
    @Override
    public Boolean updateByBo(WmUserBo bo) {
        WmUser update = BeanUtil.toBean(bo, WmUser.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmUser entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除用户
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 查看下级
     *
     * @return
     */
    @Override
    public List<WmPromUserVo> querySpread(WmUserBo bo) {
        return this.getUserSpreadGrade(bo.getUid(), 1, 999, bo.getGrade(), "", "");
    }


    /**
     * 获取我的分销下人员列表
     *
     * @param uid     uid
     * @param page    page
     * @param limit   limit
     * @param grade   ShopCommonEnum.GRADE_0
     * @param keyword 关键字搜索
     * @param sort    排序
     * @return list
     */
    @Override
    public List<WmPromUserVo> getUserSpreadGrade(Long uid, int page, int limit, Integer grade,
                                                 String keyword, String sort) {
        List<WmUser> userList = baseMapper.selectList(new LambdaQueryWrapper<WmUser>()
            .eq(WmUser::getSpreadUid, uid));
        List<Long> userIds = userList.stream()
            .map(WmUser::getUid)
            .collect(Collectors.toList());

        List<WmPromUserVo> list = new ArrayList<>();
        if (userIds.isEmpty()) {
            return list;
        }

        if (StrUtil.isBlank(sort)) {
            sort = "u.uid desc";
        }

        Page<WmUser> pageModel = new Page<>(page, limit);
        if (ShopCommonEnum.GRADE_0.getValue().equals(grade)) {//-级
            list = baseMapper.getUserSpreadCountList(pageModel, userIds,
                keyword, sort);
        } else {//二级
            List<WmUser> userListT = baseMapper.selectList(new LambdaQueryWrapper<WmUser>()
                .in(WmUser::getSpreadUid, userIds));
            List<Long> userIdsT = userListT.stream()
                .map(WmUser::getUid)
                .collect(Collectors.toList());
            if (userIdsT.isEmpty()) {
                return list;
            }
            list = baseMapper.getUserSpreadCountList(pageModel, userIdsT,
                keyword, sort);

        }
        return list;
    }

    @Override
    public void onStatus(Long id, Integer status) {
        if (ShopCommonEnum.IS_STATUS_1.getValue().equals(status)) {
            status = ShopCommonEnum.IS_STATUS_1.getValue();
        } else {
            status = ShopCommonEnum.IS_STATUS_0.getValue();
        }
        WmUser user = new WmUser();
        user.setStatus(status);
        baseMapper.update(user, new LambdaQueryWrapper<WmUser>().eq(WmUser::getUid, id));
    }

    @Override
    public void updateMoney(WmUserBo param) {
        WmUser wmUser = baseMapper.selectById(param.getUid());
        double newMoney = 0d;
        String mark = "";
        if (param.getPtype() == 1) {
            mark = "系统增加了" + param.getMoney() + "余额";
            newMoney = NumberUtil.add(wmUser.getNowMoney(), param.getMoney()).doubleValue();
            userBillService.income(wmUser.getUid(), "系统增加余额", BillDetailEnum.CATEGORY_1.getValue(),
                BillDetailEnum.TYPE_6.getValue(), param.getMoney(), newMoney, mark, "");
        } else {
            mark = "系统扣除了" + param.getMoney() + "余额";
            newMoney = NumberUtil.sub(wmUser.getNowMoney(), param.getMoney()).doubleValue();
            if (newMoney < 0) {
                newMoney = 0d;
            }
            userBillService.expend(wmUser.getUid(), "系统减少余额",
                BillDetailEnum.CATEGORY_1.getValue(),
                BillDetailEnum.TYPE_7.getValue(),
                param.getMoney(), newMoney, mark);
        }
        wmUser.setNowMoney(BigDecimal.valueOf(newMoney));
        baseMapper.insertOrUpdate(wmUser);
    }

    @Override
    public WmUserVo selectByUid(Long uid) {
        return baseMapper.selectVoOne(new LambdaQueryWrapper<WmUser>().eq(WmUser::getUid, uid));
    }

}
