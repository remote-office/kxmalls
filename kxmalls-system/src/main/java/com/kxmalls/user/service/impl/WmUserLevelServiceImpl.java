package com.kxmalls.user.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.user.domain.bo.WmUserLevelBo;
import com.kxmalls.user.domain.vo.WmUserLevelVo;
import com.kxmalls.user.domain.WmUserLevel;
import com.kxmalls.user.mapper.WmUserLevelMapper;
import com.kxmalls.user.service.IWmUserLevelService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 用户等级Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-14
 */
@RequiredArgsConstructor
@Service
public class WmUserLevelServiceImpl implements IWmUserLevelService {

    private final WmUserLevelMapper baseMapper;

    /**
     * 查询用户等级
     */
    @Override
    public WmUserLevelVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询用户等级列表
     */
    @Override
    public TableDataInfo<WmUserLevelVo> queryPageList(WmUserLevelBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmUserLevel> lqw = buildQueryWrapper(bo);
        Page<WmUserLevelVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询用户等级列表
     */
    @Override
    public List<WmUserLevelVo> queryList(WmUserLevelBo bo) {
        LambdaQueryWrapper<WmUserLevel> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmUserLevel> buildQueryWrapper(WmUserLevelBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmUserLevel> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getMerId() != null, WmUserLevel::getMerId, bo.getMerId());
        lqw.like(StringUtils.isNotBlank(bo.getName()), WmUserLevel::getName, bo.getName());
        lqw.eq(bo.getMoney() != null, WmUserLevel::getMoney, bo.getMoney());
        lqw.eq(bo.getValidDate() != null, WmUserLevel::getValidDate, bo.getValidDate());
        lqw.eq(bo.getIsForever() != null, WmUserLevel::getIsForever, bo.getIsForever());
        lqw.eq(bo.getIsPay() != null, WmUserLevel::getIsPay, bo.getIsPay());
        lqw.eq(bo.getIsShow() != null, WmUserLevel::getIsShow, bo.getIsShow());
        lqw.eq(bo.getGrade() != null, WmUserLevel::getGrade, bo.getGrade());
        lqw.eq(bo.getDiscount() != null, WmUserLevel::getDiscount, bo.getDiscount());
        lqw.eq(StringUtils.isNotBlank(bo.getImage()), WmUserLevel::getImage, bo.getImage());
        lqw.eq(StringUtils.isNotBlank(bo.getIcon()), WmUserLevel::getIcon, bo.getIcon());
        lqw.eq(StringUtils.isNotBlank(bo.getExplain()), WmUserLevel::getExplain, bo.getExplain());
        lqw.eq(bo.getIsDel() != null, WmUserLevel::getIsDel, bo.getIsDel());
        return lqw;
    }

    /**
     * 新增用户等级
     */
    @Override
    public Boolean insertByBo(WmUserLevelBo bo) {
        WmUserLevel add = BeanUtil.toBean(bo, WmUserLevel.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改用户等级
     */
    @Override
    public Boolean updateByBo(WmUserLevelBo bo) {
        WmUserLevel update = BeanUtil.toBean(bo, WmUserLevel.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmUserLevel entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除用户等级
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
