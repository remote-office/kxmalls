package com.kxmalls.user.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.user.domain.bo.WmUserLevelSettingBo;
import com.kxmalls.user.domain.vo.WmUserLevelSettingVo;
import com.kxmalls.user.domain.WmUserLevelSetting;
import com.kxmalls.user.mapper.WmUserLevelSettingMapper;
import com.kxmalls.user.service.IWmUserLevelSettingService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 设置用户等级Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-21
 */
@RequiredArgsConstructor
@Service
public class WmUserLevelSettingServiceImpl implements IWmUserLevelSettingService {

    private final WmUserLevelSettingMapper baseMapper;

    /**
     * 查询设置用户等级
     */
    @Override
    public WmUserLevelSettingVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询设置用户等级列表
     */
    @Override
    public TableDataInfo<WmUserLevelSettingVo> queryPageList(WmUserLevelSettingBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmUserLevelSetting> lqw = buildQueryWrapper(bo);
        Page<WmUserLevelSettingVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询设置用户等级列表
     */
    @Override
    public List<WmUserLevelSettingVo> queryList(WmUserLevelSettingBo bo) {
        LambdaQueryWrapper<WmUserLevelSetting> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmUserLevelSetting> buildQueryWrapper(WmUserLevelSettingBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmUserLevelSetting> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getMerId() != null, WmUserLevelSetting::getMerId, bo.getMerId());
        lqw.like(StringUtils.isNotBlank(bo.getName()), WmUserLevelSetting::getName, bo.getName());
        lqw.eq(bo.getMoney() != null, WmUserLevelSetting::getMoney, bo.getMoney());
        lqw.eq(bo.getValidDate() != null, WmUserLevelSetting::getValidDate, bo.getValidDate());
        lqw.eq(bo.getIsForever() != null, WmUserLevelSetting::getIsForever, bo.getIsForever());
        lqw.eq(bo.getIsPay() != null, WmUserLevelSetting::getIsPay, bo.getIsPay());
        lqw.eq(bo.getIsShow() != null, WmUserLevelSetting::getIsShow, bo.getIsShow());
        lqw.eq(bo.getGrade() != null, WmUserLevelSetting::getGrade, bo.getGrade());
        lqw.eq(bo.getDiscount() != null, WmUserLevelSetting::getDiscount, bo.getDiscount());
        lqw.eq(CollectionUtils.isNotEmpty(bo.getImage()), WmUserLevelSetting::getImage, bo.getImage());
        lqw.eq(CollectionUtils.isNotEmpty(bo.getIcon()), WmUserLevelSetting::getIcon, bo.getIcon());
        lqw.eq(StringUtils.isNotBlank(bo.getExplain()), WmUserLevelSetting::getExplain, bo.getExplain());
        lqw.eq(bo.getIsDel() != null, WmUserLevelSetting::getIsDel, bo.getIsDel());
        return lqw;
    }

    /**
     * 新增设置用户等级
     */
    @Override
    public Boolean insertByBo(WmUserLevelSettingBo bo) {
        WmUserLevelSetting add = BeanUtil.toBean(bo, WmUserLevelSetting.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改设置用户等级
     */
    @Override
    public Boolean updateByBo(WmUserLevelSettingBo bo) {
        WmUserLevelSetting update = BeanUtil.toBean(bo, WmUserLevelSetting.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmUserLevelSetting entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除设置用户等级
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
