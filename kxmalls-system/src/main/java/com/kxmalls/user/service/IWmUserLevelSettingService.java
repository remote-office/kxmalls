package com.kxmalls.user.service;

import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.user.domain.bo.WmUserLevelSettingBo;
import com.kxmalls.user.domain.vo.WmUserLevelSettingVo;

import java.util.Collection;
import java.util.List;

/**
 * 设置用户等级Service接口
 *
 * @author kxmalls
 * @date 2023-02-21
 */
public interface IWmUserLevelSettingService {

    /**
     * 查询设置用户等级
     */
    WmUserLevelSettingVo queryById(Long id);

    /**
     * 查询设置用户等级列表
     */
    TableDataInfo<WmUserLevelSettingVo> queryPageList(WmUserLevelSettingBo bo, PageQuery pageQuery);

    /**
     * 查询设置用户等级列表
     */
    List<WmUserLevelSettingVo> queryList(WmUserLevelSettingBo bo);

    /**
     * 新增设置用户等级
     */
    Boolean insertByBo(WmUserLevelSettingBo bo);

    /**
     * 修改设置用户等级
     */
    Boolean updateByBo(WmUserLevelSettingBo bo);

    /**
     * 校验并批量删除设置用户等级信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
