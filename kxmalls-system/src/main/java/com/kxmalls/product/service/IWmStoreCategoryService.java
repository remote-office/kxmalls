package com.kxmalls.product.service;

import com.kxmalls.product.domain.bo.WmStoreCategoryBo;
import com.kxmalls.product.domain.vo.WmStoreCategoryVo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 商品分类Service接口
 *
 * @author kxmalls
 * @date 2023-02-07
 */
public interface IWmStoreCategoryService {

    /**
     * 查询商品分类
     */
    WmStoreCategoryVo queryById(Long id);

    /**
     * 查询商品分类列表
     */
    TableDataInfo<WmStoreCategoryVo> queryPageList(WmStoreCategoryBo bo, PageQuery pageQuery);

    /**
     * 查询商品分类列表
     */
    List<WmStoreCategoryVo> queryList(WmStoreCategoryBo bo);

    /**
     * 新增商品分类
     */
    Boolean insertByBo(WmStoreCategoryBo bo);

    /**
     * 修改商品分类
     */
    Boolean updateByBo(WmStoreCategoryBo bo);

    /**
     * 校验并批量删除商品分类信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Long selectCountByPid(Long id);

    /**
     * 检测分类是否操过二级
     * @param pid 父级id
     * @return boolean
     */
    boolean checkCategory(Long pid);

    /**
     * 构建树形
     * @param storeCategoryVos 分类列表
     * @return map
     */
    Map<String, Object> buildTree(List<WmStoreCategoryVo> storeCategoryVos);

}
