package com.kxmalls.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.kxmalls.product.domain.WmStoreProductRule;
import com.kxmalls.product.domain.bo.WmStoreProductRuleBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.product.domain.vo.WmStoreProductRuleVo;
import com.kxmalls.product.mapper.WmStoreProductRuleMapper;
import com.kxmalls.product.service.IWmStoreProductRuleService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品规格Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@RequiredArgsConstructor
@Service
public class WmStoreProductRuleServiceImpl implements IWmStoreProductRuleService {

    private final WmStoreProductRuleMapper baseMapper;

    /**
     * 查询商品规格
     */
    @Override
    public WmStoreProductRuleVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品规格列表
     */
    @Override
    public TableDataInfo<WmStoreProductRuleVo> queryPageList(WmStoreProductRuleBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreProductRule> lqw = buildQueryWrapper(bo);
        Page<WmStoreProductRuleVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品规格列表
     */
    @Override
    public List<WmStoreProductRuleVo> queryList(WmStoreProductRuleBo bo) {
        LambdaQueryWrapper<WmStoreProductRule> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreProductRule> buildQueryWrapper(WmStoreProductRuleBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreProductRule> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getRuleName()), WmStoreProductRule::getRuleName, bo.getRuleName());
        return lqw;
    }

    /**
     * 新增商品规格
     */
    @Override
    public Boolean insertByBo(WmStoreProductRuleBo bo) {
        WmStoreProductRule add = BeanUtil.toBean(bo, WmStoreProductRule.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品规格
     */
    @Override
    public Boolean updateByBo(WmStoreProductRuleBo bo) {
        WmStoreProductRule update = BeanUtil.toBean(bo, WmStoreProductRule.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreProductRule entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品规格
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Integer> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<WmStoreProductRuleVo> queryListAll() {
      return baseMapper.selectVoList(null);
    }
}
