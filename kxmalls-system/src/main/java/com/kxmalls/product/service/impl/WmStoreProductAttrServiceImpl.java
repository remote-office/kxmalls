package com.kxmalls.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.kxmalls.product.domain.WmStoreProductAttr;
import com.kxmalls.product.domain.WmStoreProductAttrResult;
import com.kxmalls.product.domain.WmStoreProductAttrValue;
import com.kxmalls.product.domain.bo.WmStoreProductAttrBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.exception.ServiceException;
import com.kxmalls.common.utils.StringUtils;
import com.kxmalls.product.domain.vo.FromatDetailVo;
import com.kxmalls.product.domain.vo.ProductFormatVo;
import com.kxmalls.product.mapper.WmStoreProductAttrResultMapper;
import com.kxmalls.product.mapper.WmStoreProductAttrValueMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.product.domain.vo.WmStoreProductAttrVo;
import com.kxmalls.product.mapper.WmStoreProductAttrMapper;
import com.kxmalls.product.service.IWmStoreProductAttrService;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 商品属性Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-13
 */
@RequiredArgsConstructor
@Service
public class WmStoreProductAttrServiceImpl implements IWmStoreProductAttrService {

    private final WmStoreProductAttrMapper baseMapper;

    private final WmStoreProductAttrValueMapper storeProductAttrValueMapper;

    private final WmStoreProductAttrResultMapper storeProductAttrResultMapper;

    /**
     * 查询商品属性
     */
    @Override
    public WmStoreProductAttrVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品属性列表
     */
    @Override
    public TableDataInfo<WmStoreProductAttrVo> queryPageList(WmStoreProductAttrBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreProductAttr> lqw = buildQueryWrapper(bo);
        Page<WmStoreProductAttrVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品属性列表
     */
    @Override
    public List<WmStoreProductAttrVo> queryList(WmStoreProductAttrBo bo) {
        LambdaQueryWrapper<WmStoreProductAttr> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreProductAttr> buildQueryWrapper(WmStoreProductAttrBo bo) {
        LambdaQueryWrapper<WmStoreProductAttr> lqw = Wrappers.lambdaQuery();
        if (ObjectUtils.isEmpty(bo)) {
            return lqw;
        }
        Map<String, Object> params = bo.getParams();
        lqw.eq(bo.getProductId() != null, WmStoreProductAttr::getProductId, bo.getProductId());
        lqw.like(StringUtils.isNotBlank(bo.getAttrName()), WmStoreProductAttr::getAttrName, bo.getAttrName());
        lqw.eq(StringUtils.isNotBlank(bo.getAttrValues()), WmStoreProductAttr::getAttrValues, bo.getAttrValues());
        lqw.eq(bo.getIsDel() != null, WmStoreProductAttr::getIsDel, bo.getIsDel());
        return lqw;
    }

    /**
     * 新增商品属性
     */
    @Override
    public Boolean insertByBo(WmStoreProductAttrBo bo) {
        WmStoreProductAttr add = BeanUtil.toBean(bo, WmStoreProductAttr.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品属性
     */
    @Override
    public Boolean updateByBo(WmStoreProductAttrBo bo) {
        WmStoreProductAttr update = BeanUtil.toBean(bo, WmStoreProductAttr.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreProductAttr entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品属性
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public void insertYxStoreProductAttr(List<FromatDetailVo> items, List<ProductFormatVo> attrs, Long productId) {
        List<WmStoreProductAttr> attrGroup = new ArrayList<>();
        for (FromatDetailVo fromatDetailDto : items) {
            WmStoreProductAttr  wmStoreProductAttr = WmStoreProductAttr.builder()
                .productId(productId)
                .attrName(fromatDetailDto.getValue())
                .attrValues(StrUtil.join(",",fromatDetailDto.getDetail()))
                .build();

            attrGroup.add(wmStoreProductAttr);
        }



        List<WmStoreProductAttrValue> valueGroup = new ArrayList<>();
        for (ProductFormatVo productFormatDto : attrs) {

            if(productFormatDto.getPinkStock()>productFormatDto.getStock() || productFormatDto.getSeckillStock()>productFormatDto.getStock()){
                throw new ServiceException("活动商品库存不能大于原有商品库存");
            }
            List<String> stringList = new ArrayList<>(productFormatDto.getDetail().values());
            Collections.sort(stringList);
            WmStoreProductAttrValue oldAttrValue = storeProductAttrValueMapper.selectOne(new LambdaQueryWrapper<WmStoreProductAttrValue>()
                .eq(WmStoreProductAttrValue::getSku, productFormatDto.getSku())
                .eq(WmStoreProductAttrValue::getProductId, productId));

            String unique = IdUtil.simpleUUID();
            if (Objects.nonNull(oldAttrValue)) {
                unique = oldAttrValue.getUnique();
            }

            WmStoreProductAttrValue yxStoreProductAttrValue = WmStoreProductAttrValue.builder()
                .id(Objects.isNull(oldAttrValue) ? null : oldAttrValue.getId())
                .productId(productId)
                .sku(StrUtil.join(",",stringList))
                .price(BigDecimal.valueOf(productFormatDto.getPrice()))
                .cost(BigDecimal.valueOf(productFormatDto.getCost()))
                .otPrice(BigDecimal.valueOf(productFormatDto.getOtPrice()))
                .unique(unique)
                .image(productFormatDto.getPic())
                .barCode(productFormatDto.getBarCode())
                .weight(BigDecimal.valueOf(productFormatDto.getWeight()))
                .volume(BigDecimal.valueOf(productFormatDto.getVolume()))
                .brokerage(BigDecimal.valueOf(productFormatDto.getBrokerage()))
                .brokerageTwo(BigDecimal.valueOf(productFormatDto.getBrokerageTwo()))
                .stock(productFormatDto.getStock())
                .integral(productFormatDto.getIntegral())
                .pinkPrice(BigDecimal.valueOf(productFormatDto.getPinkPrice()==null?0:productFormatDto.getPinkPrice()))
                .seckillPrice(BigDecimal.valueOf(productFormatDto.getSeckillPrice()==null?0:productFormatDto.getSeckillPrice()))
                .pinkStock(productFormatDto.getPinkStock()==null?0:productFormatDto.getPinkStock())
                .seckillStock(productFormatDto.getSeckillStock()==null?0:productFormatDto.getSeckillStock())
                .build();

            valueGroup.add(yxStoreProductAttrValue);
        }

        if(attrGroup.isEmpty() || valueGroup.isEmpty()){
            throw new ServiceException("请设置至少一个属性!");
        }

        //清理属性
        this.clearProductAttr(productId);

        //批量添加
        baseMapper.insertBatch(attrGroup);

        storeProductAttrValueMapper.insertBatch(valueGroup);


        Map<String,Object> map = new LinkedHashMap<>();
        map.put("attr",items);
        map.put("value",attrs);

        WmStoreProductAttrResult wmStoreProductAttrResult = new WmStoreProductAttrResult();
        wmStoreProductAttrResult.setProductId(productId);
        wmStoreProductAttrResult.setResult(JSON.toJSONString(map));
        wmStoreProductAttrResult.setChangeTime(new Date());

        long count = storeProductAttrResultMapper.selectCount(new LambdaQueryWrapper<WmStoreProductAttrResult>()
            .eq(WmStoreProductAttrResult::getProductId,productId));
        if(count > 0) {
            storeProductAttrResultMapper.delete(new LambdaQueryWrapper<WmStoreProductAttrResult>()
                .eq(WmStoreProductAttrResult::getProductId,productId));
        }
        storeProductAttrResultMapper.insertOrUpdate(wmStoreProductAttrResult);
    }

    @Override
    public List<WmStoreProductAttr> queryListAll() {
        return baseMapper.selectList();
    }

    /**
     * 删除YxStoreProductAttrValue表的属性
     * @param productId 商品id
     */
    private void clearProductAttr(Long productId) {
        if(ObjectUtil.isNull(productId)) {
            throw new ServiceException("产品不存在");
        }

        baseMapper.delete(Wrappers.<WmStoreProductAttr>lambdaQuery()
            .eq(WmStoreProductAttr::getProductId,productId));
        storeProductAttrValueMapper.delete(Wrappers.<WmStoreProductAttrValue>lambdaQuery()
            .eq(WmStoreProductAttrValue::getProductId,productId));

    }

}
