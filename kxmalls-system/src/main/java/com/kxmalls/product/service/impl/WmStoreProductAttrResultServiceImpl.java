package com.kxmalls.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.kxmalls.product.domain.WmStoreProductAttrResult;
import com.kxmalls.product.domain.bo.WmStoreProductAttrResultBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.product.domain.vo.WmStoreProductAttrResultVo;
import com.kxmalls.product.mapper.WmStoreProductAttrResultMapper;
import com.kxmalls.product.service.IWmStoreProductAttrResultService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品属性详情Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-13
 */
@RequiredArgsConstructor
@Service
public class WmStoreProductAttrResultServiceImpl implements IWmStoreProductAttrResultService {

    private final WmStoreProductAttrResultMapper baseMapper;

    /**
     * 查询商品属性详情
     */
    @Override
    public WmStoreProductAttrResultVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品属性详情列表
     */
    @Override
    public TableDataInfo<WmStoreProductAttrResultVo> queryPageList(WmStoreProductAttrResultBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreProductAttrResult> lqw = buildQueryWrapper(bo);
        Page<WmStoreProductAttrResultVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品属性详情列表
     */
    @Override
    public List<WmStoreProductAttrResultVo> queryList(WmStoreProductAttrResultBo bo) {
        LambdaQueryWrapper<WmStoreProductAttrResult> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreProductAttrResult> buildQueryWrapper(WmStoreProductAttrResultBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreProductAttrResult> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getProductId() != null, WmStoreProductAttrResult::getProductId, bo.getProductId());
        lqw.eq(StringUtils.isNotBlank(bo.getResult()), WmStoreProductAttrResult::getResult, bo.getResult());
        lqw.eq(bo.getChangeTime() != null, WmStoreProductAttrResult::getChangeTime, bo.getChangeTime());
        return lqw;
    }

    /**
     * 新增商品属性详情
     */
    @Override
    public Boolean insertByBo(WmStoreProductAttrResultBo bo) {
        WmStoreProductAttrResult add = BeanUtil.toBean(bo, WmStoreProductAttrResult.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品属性详情
     */
    @Override
    public Boolean updateByBo(WmStoreProductAttrResultBo bo) {
        WmStoreProductAttrResult update = BeanUtil.toBean(bo, WmStoreProductAttrResult.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreProductAttrResult entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品属性详情
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
