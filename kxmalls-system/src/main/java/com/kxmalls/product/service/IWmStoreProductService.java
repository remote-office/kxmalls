package com.kxmalls.product.service;

import com.kxmalls.product.domain.bo.WmStoreProductBo;
import com.kxmalls.product.domain.vo.WmStoreProductVo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 商品Service接口
 *
 * @author kxmalls
 * @date 2023-02-13
 */
public interface IWmStoreProductService {

    /**
     * 查询商品
     */
    Map<String,Object> queryById(Long id);

    /**
     * 查询商品列表
     */
    TableDataInfo<WmStoreProductVo> queryPageList(WmStoreProductBo bo, PageQuery pageQuery);

    /**
     * 查询商品列表
     */
    List<WmStoreProductVo> queryList(WmStoreProductBo bo);

    /**
     * 新增商品
     */
    Boolean insertAndupdateByBo(WmStoreProductBo bo);

    /**
     * 修改商品
     */
    Boolean updateByBo(WmStoreProductBo bo);

    /**
     * 校验并批量删除商品信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Long selectCountByCateId(Long CateId);

    /**
     * 商品上下架
     * @param id
     * @param status
     */
    void onSale(Long id, Integer status);

    Map<String, Object> getFormatAttr(Long id, String jsonStr, boolean isActivity);
}
