package com.kxmalls.product.service;

import com.kxmalls.product.domain.bo.WmStoreProductAttrResultBo;
import com.kxmalls.product.domain.vo.WmStoreProductAttrResultVo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品属性详情Service接口
 *
 * @author kxmalls
 * @date 2023-02-13
 */
public interface IWmStoreProductAttrResultService {

    /**
     * 查询商品属性详情
     */
    WmStoreProductAttrResultVo queryById(Long id);

    /**
     * 查询商品属性详情列表
     */
    TableDataInfo<WmStoreProductAttrResultVo> queryPageList(WmStoreProductAttrResultBo bo, PageQuery pageQuery);

    /**
     * 查询商品属性详情列表
     */
    List<WmStoreProductAttrResultVo> queryList(WmStoreProductAttrResultBo bo);

    /**
     * 新增商品属性详情
     */
    Boolean insertByBo(WmStoreProductAttrResultBo bo);

    /**
     * 修改商品属性详情
     */
    Boolean updateByBo(WmStoreProductAttrResultBo bo);

    /**
     * 校验并批量删除商品属性详情信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
