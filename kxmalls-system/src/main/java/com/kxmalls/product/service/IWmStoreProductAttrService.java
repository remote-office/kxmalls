package com.kxmalls.product.service;

import com.kxmalls.product.domain.WmStoreProductAttr;
import com.kxmalls.product.domain.bo.WmStoreProductAttrBo;
import com.kxmalls.product.domain.vo.FromatDetailVo;
import com.kxmalls.product.domain.vo.ProductFormatVo;
import com.kxmalls.product.domain.vo.WmStoreProductAttrVo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品属性Service接口
 *
 * @author kxmalls
 * @date 2023-02-13
 */
public interface IWmStoreProductAttrService {

    /**
     * 查询商品属性
     */
    WmStoreProductAttrVo queryById(Long id);

    /**
     * 查询商品属性列表
     */
    TableDataInfo<WmStoreProductAttrVo> queryPageList(WmStoreProductAttrBo bo, PageQuery pageQuery);

    /**
     * 查询商品属性列表
     */
    List<WmStoreProductAttrVo> queryList(WmStoreProductAttrBo bo);

    /**
     * 新增商品属性
     */
    Boolean insertByBo(WmStoreProductAttrBo bo);

    /**
     * 修改商品属性
     */
    Boolean updateByBo(WmStoreProductAttrBo bo);

    /**
     * 校验并批量删除商品属性信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    void insertYxStoreProductAttr(List<FromatDetailVo> toList, List<ProductFormatVo> toList1, Long id);

    List<WmStoreProductAttr> queryListAll();
}
