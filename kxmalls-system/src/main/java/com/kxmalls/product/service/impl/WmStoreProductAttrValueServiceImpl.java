package com.kxmalls.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.kxmalls.product.domain.WmStoreProductAttrValue;
import com.kxmalls.product.domain.bo.WmStoreProductAttrValueBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.product.domain.vo.WmStoreProductAttrValueVo;
import com.kxmalls.product.mapper.WmStoreProductAttrValueMapper;
import com.kxmalls.product.service.IWmStoreProductAttrValueService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品属性值Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-13
 */
@RequiredArgsConstructor
@Service
public class WmStoreProductAttrValueServiceImpl implements IWmStoreProductAttrValueService {

    private final WmStoreProductAttrValueMapper baseMapper;

    /**
     * 查询商品属性值
     */
    @Override
    public WmStoreProductAttrValueVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品属性值列表
     */
    @Override
    public TableDataInfo<WmStoreProductAttrValueVo> queryPageList(WmStoreProductAttrValueBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreProductAttrValue> lqw = buildQueryWrapper(bo);
        Page<WmStoreProductAttrValueVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品属性值列表
     */
    @Override
    public List<WmStoreProductAttrValueVo> queryList(WmStoreProductAttrValueBo bo) {
        LambdaQueryWrapper<WmStoreProductAttrValue> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreProductAttrValue> buildQueryWrapper(WmStoreProductAttrValueBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreProductAttrValue> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getProductId() != null, WmStoreProductAttrValue::getProductId, bo.getProductId());
        lqw.eq(StringUtils.isNotBlank(bo.getSku()), WmStoreProductAttrValue::getSku, bo.getSku());
        lqw.eq(bo.getStock() != null, WmStoreProductAttrValue::getStock, bo.getStock());
        lqw.eq(bo.getSales() != null, WmStoreProductAttrValue::getSales, bo.getSales());
        lqw.eq(bo.getPrice() != null, WmStoreProductAttrValue::getPrice, bo.getPrice());
        lqw.eq(StringUtils.isNotBlank(bo.getImage()), WmStoreProductAttrValue::getImage, bo.getImage());
        lqw.eq(StringUtils.isNotBlank(bo.getUnique()), WmStoreProductAttrValue::getUnique, bo.getUnique());
        lqw.eq(bo.getCost() != null, WmStoreProductAttrValue::getCost, bo.getCost());
        lqw.eq(StringUtils.isNotBlank(bo.getBarCode()), WmStoreProductAttrValue::getBarCode, bo.getBarCode());
        lqw.eq(bo.getOtPrice() != null, WmStoreProductAttrValue::getOtPrice, bo.getOtPrice());
        lqw.eq(bo.getWeight() != null, WmStoreProductAttrValue::getWeight, bo.getWeight());
        lqw.eq(bo.getVolume() != null, WmStoreProductAttrValue::getVolume, bo.getVolume());
        lqw.eq(bo.getBrokerage() != null, WmStoreProductAttrValue::getBrokerage, bo.getBrokerage());
        lqw.eq(bo.getBrokerageTwo() != null, WmStoreProductAttrValue::getBrokerageTwo, bo.getBrokerageTwo());
        lqw.eq(bo.getPinkPrice() != null, WmStoreProductAttrValue::getPinkPrice, bo.getPinkPrice());
        lqw.eq(bo.getPinkStock() != null, WmStoreProductAttrValue::getPinkStock, bo.getPinkStock());
        lqw.eq(bo.getSeckillPrice() != null, WmStoreProductAttrValue::getSeckillPrice, bo.getSeckillPrice());
        lqw.eq(bo.getSeckillStock() != null, WmStoreProductAttrValue::getSeckillStock, bo.getSeckillStock());
        lqw.eq(bo.getIntegral() != null, WmStoreProductAttrValue::getIntegral, bo.getIntegral());
        return lqw;
    }

    /**
     * 新增商品属性值
     */
    @Override
    public Boolean insertByBo(WmStoreProductAttrValueBo bo) {
        WmStoreProductAttrValue add = BeanUtil.toBean(bo, WmStoreProductAttrValue.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品属性值
     */
    @Override
    public Boolean updateByBo(WmStoreProductAttrValueBo bo) {
        WmStoreProductAttrValue update = BeanUtil.toBean(bo, WmStoreProductAttrValue.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreProductAttrValue entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品属性值
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
