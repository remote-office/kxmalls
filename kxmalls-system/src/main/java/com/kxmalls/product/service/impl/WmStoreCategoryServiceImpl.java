package com.kxmalls.product.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.kxmalls.product.domain.WmStoreCategory;
import com.kxmalls.product.domain.bo.WmStoreCategoryBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.product.domain.vo.WmStoreCategoryVo;
import com.kxmalls.product.mapper.WmStoreCategoryMapper;
import com.kxmalls.product.service.IWmStoreCategoryService;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 商品分类Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-07
 */
@RequiredArgsConstructor
@Service
public class WmStoreCategoryServiceImpl implements IWmStoreCategoryService {

    private final WmStoreCategoryMapper baseMapper;

    /**
     * 查询商品分类
     */
    @Override
    public WmStoreCategoryVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品分类列表
     */
    @Override
    public TableDataInfo<WmStoreCategoryVo> queryPageList(WmStoreCategoryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreCategory> lqw = buildQueryWrapper(bo);
        Page<WmStoreCategoryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品分类列表
     */
    @Override
    public List<WmStoreCategoryVo> queryList(WmStoreCategoryBo bo) {
        LambdaQueryWrapper<WmStoreCategory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreCategory> buildQueryWrapper(WmStoreCategoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreCategory> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getPid() != null, WmStoreCategory::getPid, bo.getPid());
        lqw.like(StringUtils.isNotBlank(bo.getCateName()), WmStoreCategory::getCateName, bo.getCateName());
        lqw.eq(bo.getSort() != null, WmStoreCategory::getSort, bo.getSort());
        lqw.eq(StringUtils.isNotBlank(bo.getPic()), WmStoreCategory::getPic, bo.getPic());
        lqw.eq(bo.getIsShow() != null, WmStoreCategory::getIsShow, bo.getIsShow());
        return lqw;
    }

    /**
     * 新增商品分类
     */
    @Override
    public Boolean insertByBo(WmStoreCategoryBo bo) {
        WmStoreCategory add = BeanUtil.toBean(bo, WmStoreCategory.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品分类
     */
    @Override
    public Boolean updateByBo(WmStoreCategoryBo bo) {
        WmStoreCategory update = BeanUtil.toBean(bo, WmStoreCategory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreCategory entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品分类
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public Long selectCountByPid(Long id) {
        return baseMapper.selectCount(new LambdaQueryWrapper<WmStoreCategory>()
            .eq(WmStoreCategory::getPid, id));
    }

    @Override
    public boolean checkCategory(Long pid) {
        if(pid == 0) {
            return true;
        }
        WmStoreCategory wmStoreCategory = baseMapper.selectOne(new LambdaQueryWrapper<WmStoreCategory>()
            .eq(WmStoreCategory::getId,pid));
        return wmStoreCategory.getPid() <= 0;
    }

    @Override
    public Map<String, Object> buildTree(List<WmStoreCategoryVo> storeCategoryVos) {
        Set<WmStoreCategoryVo> trees = new LinkedHashSet<>();
        Set<WmStoreCategoryVo> cates = new LinkedHashSet<>();
        List<String> deptNames = storeCategoryVos.stream().map(WmStoreCategoryVo::getCateName)
            .collect(Collectors.toList());

        boolean isChild;
        List<WmStoreCategory> categories = baseMapper.selectList();
        for (WmStoreCategoryVo deptDTO : storeCategoryVos) {
            deptDTO.setLabel(deptDTO.getCateName());
            isChild = false;
            if ("0".equals(deptDTO.getPid().toString())) {
                trees.add(deptDTO);
            }
            for (WmStoreCategoryVo it : storeCategoryVos) {
                if (it.getPid().equals(deptDTO.getId())) {
                    isChild = true;
                    if (deptDTO.getChildren() == null) {
                        deptDTO.setChildren(new ArrayList<WmStoreCategoryVo>());
                    }
                    deptDTO.getChildren().add(it);
                }
            }
            if (isChild) {
                cates.add(deptDTO);
            }
            for (WmStoreCategory category : categories) {
                if (category.getId().equals(deptDTO.getPid()) && !deptNames.contains(category.getCateName())) {
                    cates.add(deptDTO);
                }
            }
        }


        if (CollectionUtils.isEmpty(trees)) {
            trees = cates;
        }
        Integer totalElements = storeCategoryVos.size();

        Map<String, Object> map = new HashMap<>(2);
        map.put("totalElements", totalElements);
        map.put("content", CollectionUtils.isEmpty(trees) ? storeCategoryVos : trees);
        return map;
    }
}
