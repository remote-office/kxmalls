package com.kxmalls.product.service;

import com.kxmalls.product.domain.bo.WmStoreProductRuleBo;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.product.domain.vo.WmStoreProductRuleVo;

import java.util.Collection;
import java.util.List;

/**
 * 商品规格Service接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface IWmStoreProductRuleService {

    /**
     * 查询商品规格
     */
    WmStoreProductRuleVo queryById(Long id);

    /**
     * 查询商品规格列表
     */
    TableDataInfo<WmStoreProductRuleVo> queryPageList(WmStoreProductRuleBo bo, PageQuery pageQuery);

    /**
     * 查询商品规格列表
     */
    List<WmStoreProductRuleVo> queryList(WmStoreProductRuleBo bo);

    /**
     * 新增商品规格
     */
    Boolean insertByBo(WmStoreProductRuleBo bo);

    /**
     * 修改商品规格
     */
    Boolean updateByBo(WmStoreProductRuleBo bo);

    /**
     * 校验并批量删除商品规格信息
     */
    Boolean deleteWithValidByIds(Collection<Integer> ids, Boolean isValid);

    List<WmStoreProductRuleVo> queryListAll();
}
