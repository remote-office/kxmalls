package com.kxmalls.product.service;

import com.kxmalls.product.domain.bo.WmStoreProductAttrValueBo;
import com.kxmalls.product.domain.vo.WmStoreProductAttrValueVo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品属性值Service接口
 *
 * @author kxmalls
 * @date 2023-02-13
 */
public interface IWmStoreProductAttrValueService {

    /**
     * 查询商品属性值
     */
    WmStoreProductAttrValueVo queryById(Long id);

    /**
     * 查询商品属性值列表
     */
    TableDataInfo<WmStoreProductAttrValueVo> queryPageList(WmStoreProductAttrValueBo bo, PageQuery pageQuery);

    /**
     * 查询商品属性值列表
     */
    List<WmStoreProductAttrValueVo> queryList(WmStoreProductAttrValueBo bo);

    /**
     * 新增商品属性值
     */
    Boolean insertByBo(WmStoreProductAttrValueBo bo);

    /**
     * 修改商品属性值
     */
    Boolean updateByBo(WmStoreProductAttrValueBo bo);

    /**
     * 校验并批量删除商品属性值信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
