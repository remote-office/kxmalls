package com.kxmalls.product.mapper;

import com.kxmalls.product.domain.WmStoreProductAttrResult;
import com.kxmalls.product.domain.vo.WmStoreProductAttrResultVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 商品属性详情Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-13
 */
public interface WmStoreProductAttrResultMapper extends BaseMapperPlus<WmStoreProductAttrResultMapper, WmStoreProductAttrResult, WmStoreProductAttrResultVo> {

}
