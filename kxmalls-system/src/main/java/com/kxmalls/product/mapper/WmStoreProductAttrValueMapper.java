package com.kxmalls.product.mapper;

import com.kxmalls.product.domain.WmStoreProductAttrValue;
import com.kxmalls.product.domain.vo.WmStoreProductAttrValueVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 商品属性值Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-13
 */
public interface WmStoreProductAttrValueMapper extends BaseMapperPlus<WmStoreProductAttrValueMapper, WmStoreProductAttrValue, WmStoreProductAttrValueVo> {

}
