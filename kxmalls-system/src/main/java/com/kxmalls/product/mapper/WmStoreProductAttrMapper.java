package com.kxmalls.product.mapper;

import com.kxmalls.product.domain.WmStoreProductAttr;
import com.kxmalls.product.domain.vo.WmStoreProductAttrVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 商品属性Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-13
 */
public interface WmStoreProductAttrMapper extends BaseMapperPlus<WmStoreProductAttrMapper, WmStoreProductAttr, WmStoreProductAttrVo> {

}
