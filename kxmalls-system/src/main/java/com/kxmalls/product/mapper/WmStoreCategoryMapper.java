package com.kxmalls.product.mapper;

import com.kxmalls.product.domain.WmStoreCategory;
import com.kxmalls.product.domain.vo.WmStoreCategoryVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 商品分类Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-07
 */
public interface WmStoreCategoryMapper extends BaseMapperPlus<WmStoreCategoryMapper, WmStoreCategory, WmStoreCategoryVo> {

}
