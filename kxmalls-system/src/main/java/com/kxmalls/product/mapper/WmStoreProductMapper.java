package com.kxmalls.product.mapper;

import com.kxmalls.product.domain.WmStoreProduct;
import com.kxmalls.product.domain.vo.WmStoreProductVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 商品Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-13
 */
public interface WmStoreProductMapper extends BaseMapperPlus<WmStoreProductMapper, WmStoreProduct, WmStoreProductVo> {

}
