package com.kxmalls.product.mapper;

import com.kxmalls.product.domain.WmStoreProductRule;
import com.kxmalls.product.domain.vo.WmStoreProductRuleVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 商品规格Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-08
 */
public interface WmStoreProductRuleMapper extends BaseMapperPlus<WmStoreProductRuleMapper, WmStoreProductRule, WmStoreProductRuleVo> {

}
