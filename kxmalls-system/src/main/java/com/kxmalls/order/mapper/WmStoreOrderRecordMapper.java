package com.kxmalls.order.mapper;

import com.kxmalls.order.domain.WmStoreOrderRecord;
import com.kxmalls.order.domain.vo.WmStoreOrderRecordVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 订单操作记录Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-15
 */
public interface WmStoreOrderRecordMapper extends BaseMapperPlus<WmStoreOrderRecordMapper, WmStoreOrderRecord, WmStoreOrderRecordVo> {

}
