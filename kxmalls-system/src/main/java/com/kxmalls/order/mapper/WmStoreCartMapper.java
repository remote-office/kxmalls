package com.kxmalls.order.mapper;

import com.kxmalls.order.domain.WmStoreCart;
import com.kxmalls.order.domain.vo.WmStoreCartVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 购物车Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-15
 */
public interface WmStoreCartMapper extends BaseMapperPlus<WmStoreCartMapper, WmStoreCart, WmStoreCartVo> {

}
