package com.kxmalls.order.mapper;

import com.kxmalls.order.domain.WmStoreOrder;
import com.kxmalls.order.domain.vo.WmStoreOrderVo;
import com.kxmalls.common.core.mapper.BaseMapperPlus;

/**
 * 订单Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-15
 */
public interface WmStoreOrderMapper extends BaseMapperPlus<WmStoreOrderMapper, WmStoreOrder, WmStoreOrderVo> {

}
