package com.kxmalls.order.service;

import com.kxmalls.order.domain.vo.WmStoreCartVo;
import com.kxmalls.order.domain.bo.WmStoreCartBo;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 购物车Service接口
 *
 * @author kxmalls
 * @date 2023-02-15
 */
public interface IWmStoreCartService {

    /**
     * 查询购物车
     */
    WmStoreCartVo queryById(Long id);

    /**
     * 查询购物车列表
     */
    TableDataInfo<WmStoreCartVo> queryPageList(WmStoreCartBo bo, PageQuery pageQuery);

    /**
     * 查询购物车列表
     */
    List<WmStoreCartVo> queryList(WmStoreCartBo bo);

    /**
     * 新增购物车
     */
    Boolean insertByBo(WmStoreCartBo bo);

    /**
     * 修改购物车
     */
    Boolean updateByBo(WmStoreCartBo bo);

    /**
     * 校验并批量删除购物车信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
