package com.kxmalls.order.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kxmalls.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.kxmalls.order.domain.bo.WmStoreCartBo;
import com.kxmalls.order.domain.vo.WmStoreCartVo;
import com.kxmalls.order.domain.WmStoreCart;
import com.kxmalls.order.mapper.WmStoreCartMapper;
import com.kxmalls.order.service.IWmStoreCartService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 购物车Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-15
 */
@RequiredArgsConstructor
@Service
public class WmStoreCartServiceImpl implements IWmStoreCartService {

    private final WmStoreCartMapper baseMapper;

    /**
     * 查询购物车
     */
    @Override
    public WmStoreCartVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询购物车列表
     */
    @Override
    public TableDataInfo<WmStoreCartVo> queryPageList(WmStoreCartBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreCart> lqw = buildQueryWrapper(bo);
        Page<WmStoreCartVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询购物车列表
     */
    @Override
    public List<WmStoreCartVo> queryList(WmStoreCartBo bo) {
        LambdaQueryWrapper<WmStoreCart> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreCart> buildQueryWrapper(WmStoreCartBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreCart> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getOid() != null, WmStoreCart::getOid, bo.getOid());
        lqw.eq(StringUtils.isNotBlank(bo.getOrderId()), WmStoreCart::getOrderId, bo.getOrderId());
        lqw.eq(bo.getCartId() != null, WmStoreCart::getCartId, bo.getCartId());
        lqw.eq(bo.getProductId() != null, WmStoreCart::getProductId, bo.getProductId());
        lqw.eq(StringUtils.isNotBlank(bo.getCartInfo()), WmStoreCart::getCartInfo, bo.getCartInfo());
        lqw.eq(StringUtils.isNotBlank(bo.getUnique()), WmStoreCart::getUnique, bo.getUnique());
        lqw.eq(bo.getIsAfterSales() != null, WmStoreCart::getIsAfterSales, bo.getIsAfterSales());
        return lqw;
    }

    /**
     * 新增购物车
     */
    @Override
    public Boolean insertByBo(WmStoreCartBo bo) {
        WmStoreCart add = BeanUtil.toBean(bo, WmStoreCart.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改购物车
     */
    @Override
    public Boolean updateByBo(WmStoreCartBo bo) {
        WmStoreCart update = BeanUtil.toBean(bo, WmStoreCart.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreCart entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除购物车
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
