package com.kxmalls.order.service;

import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.order.domain.bo.WmStoreOrderBo;
import com.kxmalls.order.domain.vo.WmOrderNowOrderRecordVo;
import com.kxmalls.order.domain.vo.WmStoreOrderVo;

import java.util.Collection;
import java.util.List;

/**
 * 订单Service接口
 *
 * @author kxmalls
 * @date 2023-02-15
 */
public interface IWmStoreOrderService {

    /**
     * 查询订单
     */
    WmStoreOrderVo queryById(Long id);

    /**
     * 查询订单列表
     */
    TableDataInfo<WmStoreOrderVo> queryPageList(WmStoreOrderBo bo, PageQuery pageQuery);

    /**
     * 查询订单列表
     */
    List<WmStoreOrderVo> queryList(WmStoreOrderBo bo);

    /**
     * 新增订单
     */
    Boolean insertByBo(WmStoreOrderBo bo);

    /**
     * 修改订单
     */
    Boolean updateByBo(WmStoreOrderBo bo);

    /**
     * 校验并批量删除订单信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    WmOrderNowOrderRecordVo getOrderStatus(Long id);
}
