package com.kxmalls.order.service;

import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.order.domain.bo.WmStoreOrderRecordBo;
import com.kxmalls.order.domain.vo.WmStoreOrderRecordVo;

import java.util.Collection;
import java.util.List;

/**
 * 订单操作记录Service接口
 *
 * @author kxmalls
 * @date 2023-02-15
 */
public interface IWmStoreOrderRecordService {

    /**
     * 查询订单操作记录
     */
    WmStoreOrderRecordVo queryById(Long id);

    /**
     * 查询订单操作记录列表
     */
    TableDataInfo<WmStoreOrderRecordVo> queryPageList(WmStoreOrderRecordBo bo, PageQuery pageQuery);

    /**
     * 查询订单操作记录列表
     */
    List<WmStoreOrderRecordVo> queryList(WmStoreOrderRecordBo bo);

    /**
     * 新增订单操作记录
     */
    Boolean insertByBo(WmStoreOrderRecordBo bo);

    /**
     * 修改订单操作记录
     */
    Boolean updateByBo(WmStoreOrderRecordBo bo);

    /**
     * 校验并批量删除订单操作记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 查询订单记录列表
     * @param id
     * @param statusList
     * @return
     */
    List<WmStoreOrderRecordVo> queryListOrderByChangeTime(Long id, List<String> statusList);
}
