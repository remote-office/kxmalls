package com.kxmalls.order.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.order.domain.WmStoreOrderRecord;
import com.kxmalls.order.domain.bo.WmStoreOrderRecordBo;
import com.kxmalls.order.domain.vo.WmStoreOrderRecordVo;
import com.kxmalls.order.mapper.WmStoreOrderRecordMapper;
import com.kxmalls.order.service.IWmStoreOrderRecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 订单操作记录Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-15
 */
@RequiredArgsConstructor
@Service
public class WmStoreOrderRecordServiceImpl implements IWmStoreOrderRecordService {

    private final WmStoreOrderRecordMapper baseMapper;

    /**
     * 查询订单操作记录
     */
    @Override
    public WmStoreOrderRecordVo queryById(Long id) {
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询订单操作记录列表
     */
    @Override
    public TableDataInfo<WmStoreOrderRecordVo> queryPageList(WmStoreOrderRecordBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreOrderRecord> lqw = buildQueryWrapper(bo);
        Page<WmStoreOrderRecordVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询订单操作记录列表
     */
    @Override
    public List<WmStoreOrderRecordVo> queryList(WmStoreOrderRecordBo bo) {
        LambdaQueryWrapper<WmStoreOrderRecord> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreOrderRecord> buildQueryWrapper(WmStoreOrderRecordBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreOrderRecord> lqw = Wrappers.lambdaQuery();
        return lqw;
    }

    /**
     * 新增订单操作记录
     */
    @Override
    public Boolean insertByBo(WmStoreOrderRecordBo bo) {
        WmStoreOrderRecord add = BeanUtil.toBean(bo, WmStoreOrderRecord.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改订单操作记录
     */
    @Override
    public Boolean updateByBo(WmStoreOrderRecordBo bo) {
        WmStoreOrderRecord update = BeanUtil.toBean(bo, WmStoreOrderRecord.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreOrderRecord entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除订单操作记录
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<WmStoreOrderRecordVo> queryListOrderByChangeTime(Long id, List<String> statusList) {
        return baseMapper.selectVoList(new LambdaQueryWrapper<WmStoreOrderRecord>().eq(WmStoreOrderRecord::getOid, id).in(WmStoreOrderRecord::getChangeType, statusList).orderByDesc(WmStoreOrderRecord::getChangeTime));
    }
}
