package com.kxmalls.order.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 购物车视图对象 wm_store_cart
 *
 * @author kxmalls
 * @date 2023-02-15
 */
@Data
@ExcelIgnoreUnannotated
public class WmStoreCartVo {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 订单id
     */
    @ExcelProperty(value = "订单id")
    private Long oid;

    /**
     * 订单号
     */
    @ExcelProperty(value = "订单号")
    private String orderId;

    /**
     * 购物车id
     */
    @ExcelProperty(value = "购物车id")
    private Long cartId;

    /**
     * 商品ID
     */
    @ExcelProperty(value = "商品ID")
    private Long productId;

    /**
     * 购买东西的详细信息
     */
    @ExcelProperty(value = "购买东西的详细信息")
    private String cartInfo;

    /**
     * 唯一id
     */
    @ExcelProperty(value = "唯一id")
    private String unique;

    /**
     * 是否能售后0不能1能
     */
    @ExcelProperty(value = "是否能售后0不能1能")
    private Integer isAfterSales;


}
