package com.kxmalls.order.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 订单操作记录视图对象 wm_store_order_record
 *
 * @author kxmalls
 * @date 2023-02-15
 */
@Data
@ExcelIgnoreUnannotated
public class WmStoreOrderRecordVo {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 订单id
     */
    @ExcelProperty(value = "订单id")
    private Long oid;

    /**
     * 操作类型
     */
    @ExcelProperty(value = "操作类型")
    private String changeType;

    /**
     * 操作备注
     */
    @ExcelProperty(value = "操作备注")
    private String changeMessage;

    /**
     * 操作时间
     */
    @ExcelProperty(value = "操作时间")
    private Date changeTime;


}
