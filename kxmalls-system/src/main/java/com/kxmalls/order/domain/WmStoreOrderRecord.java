package com.kxmalls.order.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.kxmalls.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 订单操作记录对象 wm_store_order_record
 *
 * @author kxmalls
 * @date 2023-02-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wm_store_order_record")
public class WmStoreOrderRecord extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 订单id
     */
    private Long oid;
    /**
     * 操作类型
     */
    private String changeType;
    /**
     * 操作备注
     */
    private String changeMessage;
    /**
     * 操作时间
     */
    private Date changeTime;

}
