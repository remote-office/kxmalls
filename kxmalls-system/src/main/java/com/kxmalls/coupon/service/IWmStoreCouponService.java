package com.kxmalls.coupon.service;

import com.kxmalls.coupon.domain.bo.WmStoreCouponBo;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponVo;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 优惠券Service接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface IWmStoreCouponService {

    /**
     * 查询优惠券
     */
    WmStoreCouponVo queryById(Long id);

    /**
     * 查询优惠券列表
     */
    TableDataInfo<WmStoreCouponVo> queryPageList(WmStoreCouponBo bo, PageQuery pageQuery);

    /**
     * 查询优惠券列表
     */
    List<WmStoreCouponVo> queryList(WmStoreCouponBo bo);

    /**
     * 新增优惠券
     */
    Boolean insertByBo(WmStoreCouponBo bo);

    /**
     * 修改优惠券
     */
    Boolean updateByBo(WmStoreCouponBo bo);

    /**
     * 校验并批量删除优惠券信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Map<String,Object> queryAllForSelect(WmStoreCouponBo bo);
}
