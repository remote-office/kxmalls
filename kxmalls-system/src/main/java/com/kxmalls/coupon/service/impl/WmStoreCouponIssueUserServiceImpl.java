package com.kxmalls.coupon.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kxmalls.coupon.domain.WmStoreCouponIssueUser;
import com.kxmalls.coupon.domain.bo.WmStoreCouponIssueUserBo;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponIssueUserVo;
import com.kxmalls.coupon.mapper.WmStoreCouponIssueUserMapper;
import com.kxmalls.coupon.service.IWmStoreCouponIssueUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 优惠券前台用户领取记录Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-17
 */
@RequiredArgsConstructor
@Service
public class WmStoreCouponIssueUserServiceImpl implements IWmStoreCouponIssueUserService {

    private final WmStoreCouponIssueUserMapper baseMapper;

    /**
     * 查询优惠券前台用户领取记录
     */
    @Override
    public WmStoreCouponIssueUserVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询优惠券前台用户领取记录列表
     */
    @Override
    public TableDataInfo<WmStoreCouponIssueUserVo> queryPageList(WmStoreCouponIssueUserBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreCouponIssueUser> lqw = buildQueryWrapper(bo);
        Page<WmStoreCouponIssueUserVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询优惠券前台用户领取记录列表
     */
    @Override
    public List<WmStoreCouponIssueUserVo> queryList(WmStoreCouponIssueUserBo bo) {
        LambdaQueryWrapper<WmStoreCouponIssueUser> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreCouponIssueUser> buildQueryWrapper(WmStoreCouponIssueUserBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreCouponIssueUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUid() != null, WmStoreCouponIssueUser::getUid, bo.getUid());
        lqw.eq(bo.getIssueCouponId() != null, WmStoreCouponIssueUser::getIssueCouponId, bo.getIssueCouponId());
        lqw.eq(bo.getIsDel() != null, WmStoreCouponIssueUser::getIsDel, bo.getIsDel());
        return lqw;
    }

    /**
     * 新增优惠券前台用户领取记录
     */
    @Override
    public Boolean insertByBo(WmStoreCouponIssueUserBo bo) {
        WmStoreCouponIssueUser add = BeanUtil.toBean(bo, WmStoreCouponIssueUser.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改优惠券前台用户领取记录
     */
    @Override
    public Boolean updateByBo(WmStoreCouponIssueUserBo bo) {
        WmStoreCouponIssueUser update = BeanUtil.toBean(bo, WmStoreCouponIssueUser.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreCouponIssueUser entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除优惠券前台用户领取记录
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
