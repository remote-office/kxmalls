package com.kxmalls.coupon.service;

import com.kxmalls.coupon.domain.bo.WmStoreCouponIssueUserBo;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponIssueUserVo;

import java.util.Collection;
import java.util.List;

/**
 * 优惠券前台用户领取记录Service接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface IWmStoreCouponIssueUserService {

    /**
     * 查询优惠券前台用户领取记录
     */
    WmStoreCouponIssueUserVo queryById(Long id);

    /**
     * 查询优惠券前台用户领取记录列表
     */
    TableDataInfo<WmStoreCouponIssueUserVo> queryPageList(WmStoreCouponIssueUserBo bo, PageQuery pageQuery);

    /**
     * 查询优惠券前台用户领取记录列表
     */
    List<WmStoreCouponIssueUserVo> queryList(WmStoreCouponIssueUserBo bo);

    /**
     * 新增优惠券前台用户领取记录
     */
    Boolean insertByBo(WmStoreCouponIssueUserBo bo);

    /**
     * 修改优惠券前台用户领取记录
     */
    Boolean updateByBo(WmStoreCouponIssueUserBo bo);

    /**
     * 校验并批量删除优惠券前台用户领取记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
