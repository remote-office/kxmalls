package com.kxmalls.coupon.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kxmalls.coupon.domain.WmStoreCouponUser;
import com.kxmalls.coupon.domain.bo.WmStoreCouponUserBo;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.utils.StringUtils;
import com.kxmalls.coupon.domain.vo.WmStoreCouponUserVo;
import com.kxmalls.coupon.mapper.WmStoreCouponUserMapper;
import com.kxmalls.coupon.service.IWmStoreCouponUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 优惠券发放记录Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-17
 */
@RequiredArgsConstructor
@Service
public class WmStoreCouponUserServiceImpl implements IWmStoreCouponUserService {

    private final WmStoreCouponUserMapper baseMapper;

    /**
     * 查询优惠券发放记录
     */
    @Override
    public WmStoreCouponUserVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询优惠券发放记录列表
     */
    @Override
    public TableDataInfo<WmStoreCouponUserVo> queryPageList(WmStoreCouponUserBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreCouponUser> lqw = buildQueryWrapper(bo);
        Page<WmStoreCouponUserVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询优惠券发放记录列表
     */
    @Override
    public List<WmStoreCouponUserVo> queryList(WmStoreCouponUserBo bo) {
        LambdaQueryWrapper<WmStoreCouponUser> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreCouponUser> buildQueryWrapper(WmStoreCouponUserBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreCouponUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCid() != null, WmStoreCouponUser::getCid, bo.getCid());
        lqw.eq(bo.getUid() != null, WmStoreCouponUser::getUid, bo.getUid());
        lqw.eq(StringUtils.isNotBlank(bo.getCouponTitle()), WmStoreCouponUser::getCouponTitle, bo.getCouponTitle());
        lqw.eq(bo.getCouponPrice() != null, WmStoreCouponUser::getCouponPrice, bo.getCouponPrice());
        lqw.eq(bo.getUseMinPrice() != null, WmStoreCouponUser::getUseMinPrice, bo.getUseMinPrice());
        lqw.eq(bo.getEndTime() != null, WmStoreCouponUser::getEndTime, bo.getEndTime());
        lqw.eq(bo.getUseTime() != null, WmStoreCouponUser::getUseTime, bo.getUseTime());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), WmStoreCouponUser::getType, bo.getType());
        lqw.eq(bo.getStatus() != null, WmStoreCouponUser::getStatus, bo.getStatus());
        lqw.eq(bo.getIsFail() != null, WmStoreCouponUser::getIsFail, bo.getIsFail());
        lqw.eq(bo.getIsDel() != null, WmStoreCouponUser::getIsDel, bo.getIsDel());
        return lqw;
    }

    /**
     * 新增优惠券发放记录
     */
    @Override
    public Boolean insertByBo(WmStoreCouponUserBo bo) {
        WmStoreCouponUser add = BeanUtil.toBean(bo, WmStoreCouponUser.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改优惠券发放记录
     */
    @Override
    public Boolean updateByBo(WmStoreCouponUserBo bo) {
        WmStoreCouponUser update = BeanUtil.toBean(bo, WmStoreCouponUser.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreCouponUser entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除优惠券发放记录
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
