package com.kxmalls.coupon.service;

import com.kxmalls.coupon.domain.bo.WmStoreCouponIssueBo;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponIssueVo;

import java.util.Collection;
import java.util.List;

/**
 * 优惠券前台领取Service接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface IWmStoreCouponIssueService {

    /**
     * 查询优惠券前台领取
     */
    WmStoreCouponIssueVo queryById(Integer id);

    /**
     * 查询优惠券前台领取列表
     */
    TableDataInfo<WmStoreCouponIssueVo> queryPageList(WmStoreCouponIssueBo bo, PageQuery pageQuery);

    /**
     * 查询优惠券前台领取列表
     */
    List<WmStoreCouponIssueVo> queryList(WmStoreCouponIssueBo bo);

    /**
     * 新增优惠券前台领取
     */
    Boolean insertByBo(WmStoreCouponIssueBo bo);

    /**
     * 修改优惠券前台领取
     */
    Boolean updateByBo(WmStoreCouponIssueBo bo);

    /**
     * 校验并批量删除优惠券前台领取信息
     */
    Boolean deleteWithValidByIds(Collection<Integer> ids, Boolean isValid);
}
