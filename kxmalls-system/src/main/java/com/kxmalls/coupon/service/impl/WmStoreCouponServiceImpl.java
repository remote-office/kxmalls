package com.kxmalls.coupon.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kxmalls.coupon.domain.WmStoreCoupon;
import com.kxmalls.coupon.domain.bo.WmStoreCouponBo;
import com.kxmalls.product.domain.WmStoreProduct;
import com.kxmalls.product.mapper.WmStoreProductMapper;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.utils.StringUtils;
import com.kxmalls.coupon.domain.vo.WmStoreCouponVo;
import com.kxmalls.coupon.mapper.WmStoreCouponMapper;
import com.kxmalls.coupon.service.IWmStoreCouponService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 优惠券Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-17
 */
@RequiredArgsConstructor
@Service
public class WmStoreCouponServiceImpl implements IWmStoreCouponService {

    private final WmStoreCouponMapper baseMapper;

    private final WmStoreProductMapper wmStoreProductMapper;

    /**
     * 查询优惠券
     */
    @Override
    public WmStoreCouponVo queryById(Long id) {
        WmStoreCouponVo wcv = baseMapper.selectVoById(id);
        if (StrUtil.isNotBlank(wcv.getProductId())) {
            List<WmStoreProduct> storeProducts = wmStoreProductMapper
                    .selectList(new LambdaQueryWrapper<WmStoreProduct>()
                            .in(WmStoreProduct::getId, Arrays.asList(wcv.getProductId().split(","))));
            wcv.setProduct(storeProducts);
        }
        return wcv;
    }

    /**
     * 查询优惠券列表
     */
    @Override
    public TableDataInfo<WmStoreCouponVo> queryPageList(WmStoreCouponBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreCoupon> lqw = buildQueryWrapper(bo);
        Page<WmStoreCouponVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        for (WmStoreCouponVo storeCouponVo : result.getRecords()) {
            if (StrUtil.isNotBlank(storeCouponVo.getProductId())) {
                List<WmStoreProduct> storeProducts = wmStoreProductMapper
                        .selectList(new LambdaQueryWrapper<WmStoreProduct>()
                                .in(WmStoreProduct::getId, Arrays.asList(storeCouponVo.getProductId().split(","))));
                storeCouponVo.setProduct(storeProducts);
            }
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询优惠券列表
     */
    @Override
    public List<WmStoreCouponVo> queryList(WmStoreCouponBo bo) {
        LambdaQueryWrapper<WmStoreCoupon> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreCoupon> buildQueryWrapper(WmStoreCouponBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreCoupon> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getTitle()), WmStoreCoupon::getTitle, bo.getTitle());
        lqw.eq(bo.getIntegral() != null, WmStoreCoupon::getIntegral, bo.getIntegral());
        lqw.eq(bo.getCouponPrice() != null, WmStoreCoupon::getCouponPrice, bo.getCouponPrice());
        lqw.eq(bo.getUseMinPrice() != null, WmStoreCoupon::getUseMinPrice, bo.getUseMinPrice());
        lqw.eq(bo.getCouponTime() != null, WmStoreCoupon::getCouponTime, bo.getCouponTime());
        lqw.eq(bo.getSort() != null, WmStoreCoupon::getSort, bo.getSort());
        lqw.eq(bo.getStatus() != null, WmStoreCoupon::getStatus, bo.getStatus());
        lqw.eq(StringUtils.isNotBlank(bo.getProductId()), WmStoreCoupon::getProductId, bo.getProductId());
        lqw.eq(bo.getType() != null, WmStoreCoupon::getType, bo.getType());
        lqw.eq(bo.getIsDel() != null, WmStoreCoupon::getIsDel, bo.getIsDel());
        return lqw;
    }

    /**
     * 新增优惠券
     */
    @Override
    public Boolean insertByBo(WmStoreCouponBo bo) {
        WmStoreCoupon add = BeanUtil.toBean(bo, WmStoreCoupon.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改优惠券
     */
    @Override
    public Boolean updateByBo(WmStoreCouponBo bo) {
        WmStoreCoupon update = BeanUtil.toBean(bo, WmStoreCoupon.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreCoupon entity) {
        // TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除优惠券
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            // TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public Map<String, Object> queryAllForSelect(WmStoreCouponBo bo) {
        List<WmStoreCouponVo> wmStoreCouponVos = queryList(bo);
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", wmStoreCouponVos);
        map.put("totalElements", wmStoreCouponVos.size());
        return map;
    }
}
