package com.kxmalls.coupon.service;

import com.kxmalls.coupon.domain.bo.WmStoreCouponUserBo;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponUserVo;

import java.util.Collection;
import java.util.List;

/**
 * 优惠券发放记录Service接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface IWmStoreCouponUserService {

    /**
     * 查询优惠券发放记录
     */
    WmStoreCouponUserVo queryById(Long id);

    /**
     * 查询优惠券发放记录列表
     */
    TableDataInfo<WmStoreCouponUserVo> queryPageList(WmStoreCouponUserBo bo, PageQuery pageQuery);

    /**
     * 查询优惠券发放记录列表
     */
    List<WmStoreCouponUserVo> queryList(WmStoreCouponUserBo bo);

    /**
     * 新增优惠券发放记录
     */
    Boolean insertByBo(WmStoreCouponUserBo bo);

    /**
     * 修改优惠券发放记录
     */
    Boolean updateByBo(WmStoreCouponUserBo bo);

    /**
     * 校验并批量删除优惠券发放记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
