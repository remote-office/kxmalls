package com.kxmalls.coupon.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kxmalls.coupon.domain.WmStoreCouponIssue;
import com.kxmalls.coupon.domain.bo.WmStoreCouponIssueBo;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.utils.StringUtils;
import com.kxmalls.coupon.domain.vo.WmStoreCouponIssueVo;
import com.kxmalls.coupon.mapper.WmStoreCouponIssueMapper;
import com.kxmalls.coupon.service.IWmStoreCouponIssueService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 优惠券前台领取Service业务层处理
 *
 * @author kxmalls
 * @date 2023-02-17
 */
@RequiredArgsConstructor
@Service
public class WmStoreCouponIssueServiceImpl implements IWmStoreCouponIssueService {

    private final WmStoreCouponIssueMapper baseMapper;

    /**
     * 查询优惠券前台领取
     */
    @Override
    public WmStoreCouponIssueVo queryById(Integer id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询优惠券前台领取列表
     */
    @Override
    public TableDataInfo<WmStoreCouponIssueVo> queryPageList(WmStoreCouponIssueBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WmStoreCouponIssue> lqw = buildQueryWrapper(bo);
        Page<WmStoreCouponIssueVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询优惠券前台领取列表
     */
    @Override
    public List<WmStoreCouponIssueVo> queryList(WmStoreCouponIssueBo bo) {
        LambdaQueryWrapper<WmStoreCouponIssue> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WmStoreCouponIssue> buildQueryWrapper(WmStoreCouponIssueBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WmStoreCouponIssue> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getCname()), WmStoreCouponIssue::getCname, bo.getCname());
        lqw.eq(bo.getCid() != null, WmStoreCouponIssue::getCid, bo.getCid());
        lqw.eq(bo.getCtype() != null, WmStoreCouponIssue::getCtype, bo.getCtype());
        lqw.eq(bo.getStartTime() != null, WmStoreCouponIssue::getStartTime, bo.getStartTime());
        lqw.eq(bo.getEndTime() != null, WmStoreCouponIssue::getEndTime, bo.getEndTime());
        lqw.eq(bo.getTotalCount() != null, WmStoreCouponIssue::getTotalCount, bo.getTotalCount());
        lqw.eq(bo.getRemainCount() != null, WmStoreCouponIssue::getRemainCount, bo.getRemainCount());
        lqw.eq(bo.getIsPermanent() != null, WmStoreCouponIssue::getIsPermanent, bo.getIsPermanent());
        lqw.eq(bo.getStatus() != null, WmStoreCouponIssue::getStatus, bo.getStatus());
        lqw.eq(bo.getIsDel() != null, WmStoreCouponIssue::getIsDel, bo.getIsDel());
        return lqw;
    }

    /**
     * 新增优惠券前台领取
     */
    @Override
    public Boolean insertByBo(WmStoreCouponIssueBo bo) {
        WmStoreCouponIssue add = BeanUtil.toBean(bo, WmStoreCouponIssue.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改优惠券前台领取
     */
    @Override
    public Boolean updateByBo(WmStoreCouponIssueBo bo) {
        WmStoreCouponIssue update = BeanUtil.toBean(bo, WmStoreCouponIssue.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WmStoreCouponIssue entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除优惠券前台领取
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Integer> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
