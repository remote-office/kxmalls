package com.kxmalls.coupon.mapper;

import com.kxmalls.coupon.domain.WmStoreCoupon;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.coupon.domain.vo.WmStoreCouponVo;

/**
 * 优惠券Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface WmStoreCouponMapper extends BaseMapperPlus<WmStoreCouponMapper, WmStoreCoupon, WmStoreCouponVo> {

}
