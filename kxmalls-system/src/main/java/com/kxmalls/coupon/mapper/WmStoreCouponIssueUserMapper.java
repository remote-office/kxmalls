package com.kxmalls.coupon.mapper;

import com.kxmalls.coupon.domain.WmStoreCouponIssueUser;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.coupon.domain.vo.WmStoreCouponIssueUserVo;

/**
 * 优惠券前台用户领取记录Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface WmStoreCouponIssueUserMapper extends BaseMapperPlus<WmStoreCouponIssueUserMapper, WmStoreCouponIssueUser, WmStoreCouponIssueUserVo> {

}
