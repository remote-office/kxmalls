package com.kxmalls.coupon.mapper;

import com.kxmalls.coupon.domain.WmStoreCouponIssue;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.coupon.domain.vo.WmStoreCouponIssueVo;

/**
 * 优惠券前台领取Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface WmStoreCouponIssueMapper extends BaseMapperPlus<WmStoreCouponIssueMapper, WmStoreCouponIssue, WmStoreCouponIssueVo> {

}
