package com.kxmalls.coupon.mapper;

import com.kxmalls.coupon.domain.WmStoreCouponUser;
import com.kxmalls.common.core.mapper.BaseMapperPlus;
import com.kxmalls.coupon.domain.vo.WmStoreCouponUserVo;

/**
 * 优惠券发放记录Mapper接口
 *
 * @author kxmalls
 * @date 2023-02-17
 */
public interface WmStoreCouponUserMapper extends BaseMapperPlus<WmStoreCouponUserMapper, WmStoreCouponUser, WmStoreCouponUserVo> {

}
