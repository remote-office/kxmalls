package com.kxmalls.web.controller.order;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.order.domain.vo.WmStoreOrderRecordVo;
import com.kxmalls.order.domain.bo.WmStoreOrderRecordBo;
import com.kxmalls.order.service.IWmStoreOrderRecordService;
import com.kxmalls.common.core.page.TableDataInfo;

/**
 * 订单操作记录
 *
 * @author kxmalls
 * @date 2023-02-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/order/storeOrderRecord")
public class WmStoreOrderRecordController extends BaseController {

    private final IWmStoreOrderRecordService iWmStoreOrderRecordService;

    /**
     * 查询订单操作记录列表
     */
    @SaCheckPermission("order:storeOrderRecord:list")
    @GetMapping("/list")
    public TableDataInfo<WmStoreOrderRecordVo> list(WmStoreOrderRecordBo bo, PageQuery pageQuery) {
        return iWmStoreOrderRecordService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出订单操作记录列表
     */
    @SaCheckPermission("order:storeOrderRecord:export")
    @Log(title = "订单操作记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmStoreOrderRecordBo bo, HttpServletResponse response) {
        List<WmStoreOrderRecordVo> list = iWmStoreOrderRecordService.queryList(bo);
        ExcelUtil.exportExcel(list, "订单操作记录", WmStoreOrderRecordVo.class, response);
    }

    /**
     * 获取订单操作记录详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("order:storeOrderRecord:query")
    @GetMapping("/{id}")
    public R<WmStoreOrderRecordVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmStoreOrderRecordService.queryById(id));
    }

    /**
     * 新增订单操作记录
     */
    @SaCheckPermission("order:storeOrderRecord:add")
    @Log(title = "订单操作记录", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmStoreOrderRecordBo bo) {
        return toAjax(iWmStoreOrderRecordService.insertByBo(bo));
    }

    /**
     * 修改订单操作记录
     */
    @SaCheckPermission("order:storeOrderRecord:edit")
    @Log(title = "订单操作记录", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmStoreOrderRecordBo bo) {
        return toAjax(iWmStoreOrderRecordService.updateByBo(bo));
    }

    /**
     * 删除订单操作记录
     *
     * @param ids 主键串
     */
    @SaCheckPermission("order:storeOrderRecord:remove")
    @Log(title = "订单操作记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmStoreOrderRecordService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
