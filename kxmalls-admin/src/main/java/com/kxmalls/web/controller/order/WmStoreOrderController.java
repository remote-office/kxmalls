package com.kxmalls.web.controller.order;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.StrUtil;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.exception.ServiceException;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.order.domain.bo.WmStoreOrderBo;
import com.kxmalls.order.domain.vo.WmOrderNowOrderRecordVo;
import com.kxmalls.order.domain.vo.WmStoreOrderVo;
import com.kxmalls.order.service.IWmStoreOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 订单
 *
 * @author kxmalls
 * @date 2023-02-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/order/storeOrder")
public class WmStoreOrderController extends BaseController {

    private final IWmStoreOrderService iWmStoreOrderService;

    /**
     * 查询订单列表
     */
    @SaCheckPermission("order:storeOrder:list")
    @GetMapping("/list")
    public TableDataInfo<WmStoreOrderVo> list(WmStoreOrderBo bo, PageQuery pageQuery) {
        if ("nickname".equals(bo.getType())) {
            bo.setRealName(bo.getValue());
        }
        if ("phone".equals(bo.getType())) {
            bo.setUserPhone(bo.getValue());
        }
        if ("orderId".equals(bo.getType())) {
            bo.setOrderId(bo.getValue());
        }
        return iWmStoreOrderService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出订单列表
     */
    @SaCheckPermission("order:storeOrder:export")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmStoreOrderBo bo, HttpServletResponse response) {
        List<WmStoreOrderVo> list = iWmStoreOrderService.queryList(bo);
        ExcelUtil.exportExcel(list, "订单", WmStoreOrderVo.class, response);
    }

    /**
     * 获取订单详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("order:storeOrder:query")
    @GetMapping("/{id}")
    public R<WmStoreOrderVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmStoreOrderService.queryById(id));
    }

    /**
     * 查询订单当前状态流程
     */
    @GetMapping(value = "/getOrderRecord/{id}")
    public R<WmOrderNowOrderRecordVo> getOrderRecord(@PathVariable Long id) {
        return R.ok(iWmStoreOrderService.getOrderStatus(id));
    }


    /**
     * 新增订单
     */
    @SaCheckPermission("order:storeOrder:add")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmStoreOrderBo bo) {
        return toAjax(iWmStoreOrderService.insertByBo(bo));
    }

    /**
     * 修改订单
     */
    @SaCheckPermission("order:storeOrder:edit")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmStoreOrderBo bo) {
        if (StrUtil.isBlank(bo.getDeliveryName())) {
            throw new ServiceException("请选择快递公司");
        }
        if (StrUtil.isBlank(bo.getDeliveryId())) {
            throw new ServiceException("快递单号不能为空");
        }
        return toAjax(iWmStoreOrderService.updateByBo(bo));
    }

    /**
     * 删除订单
     *
     * @param ids 主键串
     */
    @SaCheckPermission("order:storeOrder:remove")
    @Log(title = "订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmStoreOrderService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

}
