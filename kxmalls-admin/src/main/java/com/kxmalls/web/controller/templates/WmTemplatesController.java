package com.kxmalls.web.controller.templates;

import java.util.List;
import java.util.Arrays;

import com.kxmalls.common.constant.ShopConstants;
import com.kxmalls.system.domain.bo.SysRegionBo;
import com.kxmalls.system.domain.vo.SysRegionVo;
import com.kxmalls.system.service.ISysRegionService;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.templates.domain.vo.WmTemplatesVo;
import com.kxmalls.templates.domain.bo.WmTemplatesBo;
import com.kxmalls.templates.service.IWmTemplatesService;
import com.kxmalls.common.core.page.TableDataInfo;

/**
 * 运费模板
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/templates/templates")
public class WmTemplatesController extends BaseController {

    private final IWmTemplatesService iWmTemplatesService;
    private final ISysRegionService iSysRegionService;

    /**
     * 查询运费模板列表
     */
    @SaCheckPermission("templates:templates:list")
    @GetMapping("/list")
    public TableDataInfo<WmTemplatesVo> list(WmTemplatesBo bo, PageQuery pageQuery) {
        return iWmTemplatesService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出运费模板列表
     */
    @SaCheckPermission("templates:templates:export")
    @Log(title = "运费模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmTemplatesBo bo, HttpServletResponse response) {
        List<WmTemplatesVo> list = iWmTemplatesService.queryList(bo);
        ExcelUtil.exportExcel(list, "运费模板", WmTemplatesVo.class, response);
    }

    /**
     * 获取运费模板详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("templates:templates:query")
    @GetMapping("/{id}")
    public R<WmTemplatesVo> getInfo(@NotNull(message = "主键不能为空")
                                    @PathVariable Long id) {
        return R.ok(iWmTemplatesService.queryById(id));
    }

    /**
     * 新增运费模板
     */
    @SaCheckPermission("templates:templates:add")
    @Log(title = "运费模板", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmTemplatesBo bo) {
        return toAjax(iWmTemplatesService.insertAndupdateByBo(bo));
    }

    /**
     * 修改运费模板
     */
    @SaCheckPermission("templates:templates:edit")
    @Log(title = "运费模板", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmTemplatesBo bo) {
        return toAjax(iWmTemplatesService.insertAndupdateByBo(bo));
    }

    /**
     * 删除运费模板
     *
     * @param ids 主键串
     */
    @SaCheckPermission("templates:templates:remove")
    @Log(title = "运费模板", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {

//        List<YxStoreProduct> productList = yxStoreProductService.list();
//        Arrays.asList(ids).forEach(id->{
//            for (YxStoreProduct yxStoreProduct : productList) {
//                if(id.equals(yxStoreProduct.getTempId())){
//                    throw new ServiceException("运费模板存在商品关联，请删除对应商品");
//                }
//            }
//        });

        return toAjax(iWmTemplatesService.deleteWithValidByIds(Arrays.asList(ids), true));
    }


    /**
     * 获取城市列表
     */
    @Cacheable(cacheNames = ShopConstants.WMSHOP_REDIS_SYS_CITY_KEY)
    @GetMapping("/citys")
    public R<Object> cityList() {
        SysRegionBo bo = new SysRegionBo();
        bo.setParentId(0L);
        List<SysRegionVo> cityList = iSysRegionService.queryList(bo);

        for (SysRegionVo regionVo : cityList) {
            bo.setParentId(regionVo.getCityId());
            List<SysRegionVo> children = iSysRegionService.queryList(bo);
            regionVo.setChildren(children);
        }

        return R.ok(cityList);
    }
}
