package com.kxmalls.web.controller.coupon;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.coupon.domain.bo.WmStoreCouponIssueBo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponIssueVo;
import com.kxmalls.coupon.service.IWmStoreCouponIssueService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 优惠券前台领取
 *
 * @author kxmalls
 * @date 2023-02-17
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/coupon/storeCouponIssue")
public class WmStoreCouponIssueController extends BaseController {

    private final IWmStoreCouponIssueService iWmStoreCouponIssueService;

    /**
     * 查询优惠券前台领取列表-查询已发布
     */
    @SaCheckPermission("coupon:storeCouponIssue:list")
    @GetMapping("/list")
    public TableDataInfo<WmStoreCouponIssueVo> list(WmStoreCouponIssueBo bo, PageQuery pageQuery) {
        return iWmStoreCouponIssueService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出优惠券前台领取列表
     */
    @SaCheckPermission("coupon:storeCouponIssue:export")
    @Log(title = "优惠券前台领取", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmStoreCouponIssueBo bo, HttpServletResponse response) {
        List<WmStoreCouponIssueVo> list = iWmStoreCouponIssueService.queryList(bo);
        ExcelUtil.exportExcel(list, "优惠券前台领取", WmStoreCouponIssueVo.class, response);
    }

    /**
     * 获取优惠券前台领取详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("coupon:storeCouponIssue:query")
    @GetMapping("/{id}")
    public R<WmStoreCouponIssueVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Integer id) {
        return R.ok(iWmStoreCouponIssueService.queryById(id));
    }

    /**
     * 新增优惠券前台领取
     */
    @SaCheckPermission("coupon:storeCouponIssue:add")
    @Log(title = "优惠券前台领取", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmStoreCouponIssueBo bo) {
        if(bo.getTotalCount() > 0) {
            bo.setRemainCount(bo.getTotalCount());
        }
        return toAjax(iWmStoreCouponIssueService.insertByBo(bo));
    }

    /**
     * 修改优惠券前台领取
     */
    @SaCheckPermission("coupon:storeCouponIssue:edit")
    @Log(title = "优惠券前台领取", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmStoreCouponIssueBo bo) {
        return toAjax(iWmStoreCouponIssueService.updateByBo(bo));
    }

    /**
     * 删除优惠券前台领取
     *
     * @param ids 主键串
     */
    @SaCheckPermission("coupon:storeCouponIssue:remove")
    @Log(title = "优惠券前台领取", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Integer[] ids) {
        return toAjax(iWmStoreCouponIssueService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
