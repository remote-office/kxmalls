package com.kxmalls.web.controller.coupon;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.StrUtil;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.enums.CouponEnum;
import com.kxmalls.common.exception.ServiceException;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.coupon.domain.bo.WmStoreCouponBo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponVo;
import com.kxmalls.coupon.service.IWmStoreCouponService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 优惠券
 *
 * @author kxmalls
 * @date 2023-02-17
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/coupon/storeCoupon")
public class WmStoreCouponController extends BaseController {

    private final IWmStoreCouponService iWmStoreCouponService;

    /**
     * 查询优惠券列表
     */
    @SaCheckPermission("coupon:storeCoupon:list")
    @GetMapping("/list")
    public TableDataInfo<WmStoreCouponVo> list(WmStoreCouponBo bo, PageQuery pageQuery) {
        return iWmStoreCouponService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询所有有效优惠券
     */
    @SaCheckPermission("coupon:storeCoupon:list")
    @GetMapping("/listAll")
    public R<Map<String,Object>> listAll(WmStoreCouponBo bo) {
        return R.ok(iWmStoreCouponService.queryAllForSelect(bo));
    }

    /**
     * 导出优惠券列表
     */
    @SaCheckPermission("coupon:storeCoupon:export")
    @Log(title = "优惠券", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmStoreCouponBo bo, HttpServletResponse response) {
        List<WmStoreCouponVo> list = iWmStoreCouponService.queryList(bo);
        ExcelUtil.exportExcel(list, "优惠券", WmStoreCouponVo.class, response);
    }

    /**
     * 获取优惠券详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("coupon:storeCoupon:query")
    @GetMapping("/{id}")
    public R<WmStoreCouponVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmStoreCouponService.queryById(id));
    }

    /**
     * 新增优惠券
     */
    @SaCheckPermission("coupon:storeCoupon:add")
    @Log(title = "优惠券", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmStoreCouponBo bo) {
        if(CouponEnum.TYPE_1.getValue().equals(bo.getType())
            && StrUtil.isEmpty(bo.getProductId())){
            throw new ServiceException("请选择商品");
        }
        if(bo.getCouponPrice().compareTo(bo.getUseMinPrice()) > 0) {
            throw new ServiceException("优惠券金额不能高于最低消费金额");
        }
        return toAjax(iWmStoreCouponService.insertByBo(bo));
    }

    /**
     * 修改优惠券
     */
    @SaCheckPermission("coupon:storeCoupon:edit")
    @Log(title = "优惠券", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmStoreCouponBo bo) {
        if(CouponEnum.TYPE_1.getValue().equals(bo.getType())
            && StrUtil.isEmpty(bo.getProductId())){
            throw new ServiceException("请选择商品");
        }
        if(bo.getCouponPrice().compareTo(bo.getUseMinPrice()) > 0) {
            throw new ServiceException("优惠券金额不能高于最低消费金额");
        }
        return toAjax(iWmStoreCouponService.updateByBo(bo));
    }

    /**
     * 删除优惠券
     *
     * @param ids 主键串
     */
    @SaCheckPermission("coupon:storeCoupon:remove")
    @Log(title = "优惠券", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmStoreCouponService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
