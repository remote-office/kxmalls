package com.kxmalls.web.controller.templates;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.templates.domain.vo.WmTemplatesRegionVo;
import com.kxmalls.templates.domain.bo.WmTemplatesRegionBo;
import com.kxmalls.templates.service.IWmTemplatesRegionService;
import com.kxmalls.common.core.page.TableDataInfo;

/**
 * 运费模板区域
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/templates/templatesRegion")
public class WmTemplatesRegionController extends BaseController {

    private final IWmTemplatesRegionService iWmTemplatesRegionService;

    /**
     * 查询运费模板区域列表
     */
    @SaCheckPermission("templates:templatesRegion:list")
    @GetMapping("/list")
    public TableDataInfo<WmTemplatesRegionVo> list(WmTemplatesRegionBo bo, PageQuery pageQuery) {
        return iWmTemplatesRegionService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出运费模板区域列表
     */
    @SaCheckPermission("templates:templatesRegion:export")
    @Log(title = "运费模板区域", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmTemplatesRegionBo bo, HttpServletResponse response) {
        List<WmTemplatesRegionVo> list = iWmTemplatesRegionService.queryList(bo);
        ExcelUtil.exportExcel(list, "运费模板区域", WmTemplatesRegionVo.class, response);
    }

    /**
     * 获取运费模板区域详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("templates:templatesRegion:query")
    @GetMapping("/{id}")
    public R<WmTemplatesRegionVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmTemplatesRegionService.queryById(id));
    }

    /**
     * 新增运费模板区域
     */
    @SaCheckPermission("templates:templatesRegion:add")
    @Log(title = "运费模板区域", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmTemplatesRegionBo bo) {
        return toAjax(iWmTemplatesRegionService.insertByBo(bo));
    }

    /**
     * 修改运费模板区域
     */
    @SaCheckPermission("templates:templatesRegion:edit")
    @Log(title = "运费模板区域", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmTemplatesRegionBo bo) {
        return toAjax(iWmTemplatesRegionService.updateByBo(bo));
    }

    /**
     * 删除运费模板区域
     *
     * @param ids 主键串
     */
    @SaCheckPermission("templates:templatesRegion:remove")
    @Log(title = "运费模板区域", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmTemplatesRegionService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
