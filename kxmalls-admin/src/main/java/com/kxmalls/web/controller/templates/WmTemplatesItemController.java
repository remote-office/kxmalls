package com.kxmalls.web.controller.templates;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.templates.domain.vo.WmTemplatesItemVo;
import com.kxmalls.templates.domain.bo.WmTemplatesItemBo;
import com.kxmalls.templates.service.IWmTemplatesItemService;
import com.kxmalls.common.core.page.TableDataInfo;

/**
 * 运费模板详情
 *
 * @author kxmalls
 * @date 2023-02-08
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/templates/templatesItem")
public class WmTemplatesItemController extends BaseController {

    private final IWmTemplatesItemService iWmTemplatesItemService;

    /**
     * 查询运费模板详情列表
     */
    @SaCheckPermission("templates:templatesItem:list")
    @GetMapping("/list")
    public TableDataInfo<WmTemplatesItemVo> list(WmTemplatesItemBo bo, PageQuery pageQuery) {
        return iWmTemplatesItemService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出运费模板详情列表
     */
    @SaCheckPermission("templates:templatesItem:export")
    @Log(title = "运费模板详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmTemplatesItemBo bo, HttpServletResponse response) {
        List<WmTemplatesItemVo> list = iWmTemplatesItemService.queryList(bo);
        ExcelUtil.exportExcel(list, "运费模板详情", WmTemplatesItemVo.class, response);
    }

    /**
     * 获取运费模板详情详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("templates:templatesItem:query")
    @GetMapping("/{id}")
    public R<WmTemplatesItemVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmTemplatesItemService.queryById(id));
    }

    /**
     * 新增运费模板详情
     */
    @SaCheckPermission("templates:templatesItem:add")
    @Log(title = "运费模板详情", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmTemplatesItemBo bo) {
        return toAjax(iWmTemplatesItemService.insertByBo(bo));
    }

    /**
     * 修改运费模板详情
     */
    @SaCheckPermission("templates:templatesItem:edit")
    @Log(title = "运费模板详情", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmTemplatesItemBo bo) {
        return toAjax(iWmTemplatesItemService.updateByBo(bo));
    }

    /**
     * 删除运费模板详情
     *
     * @param ids 主键串
     */
    @SaCheckPermission("templates:templatesItem:remove")
    @Log(title = "运费模板详情", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmTemplatesItemService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
