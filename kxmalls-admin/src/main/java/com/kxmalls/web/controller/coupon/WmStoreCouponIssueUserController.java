package com.kxmalls.web.controller.coupon;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.coupon.domain.bo.WmStoreCouponIssueUserBo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponIssueUserVo;
import com.kxmalls.coupon.service.IWmStoreCouponIssueUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 优惠券前台用户领取记录
 *
 * @author kxmalls
 * @date 2023-02-17
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/coupon/storeCouponIssueUser")
public class WmStoreCouponIssueUserController extends BaseController {

    private final IWmStoreCouponIssueUserService iWmStoreCouponIssueUserService;

    /**
     * 查询优惠券前台用户领取记录列表
     */
    @SaCheckPermission("coupon:storeCouponIssueUser:list")
    @GetMapping("/list")
    public TableDataInfo<WmStoreCouponIssueUserVo> list(WmStoreCouponIssueUserBo bo, PageQuery pageQuery) {
        return iWmStoreCouponIssueUserService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出优惠券前台用户领取记录列表
     */
    @SaCheckPermission("coupon:storeCouponIssueUser:export")
    @Log(title = "优惠券前台用户领取记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmStoreCouponIssueUserBo bo, HttpServletResponse response) {
        List<WmStoreCouponIssueUserVo> list = iWmStoreCouponIssueUserService.queryList(bo);
        ExcelUtil.exportExcel(list, "优惠券前台用户领取记录", WmStoreCouponIssueUserVo.class, response);
    }

    /**
     * 获取优惠券前台用户领取记录详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("coupon:storeCouponIssueUser:query")
    @GetMapping("/{id}")
    public R<WmStoreCouponIssueUserVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmStoreCouponIssueUserService.queryById(id));
    }

    /**
     * 新增优惠券前台用户领取记录
     */
    @SaCheckPermission("coupon:storeCouponIssueUser:add")
    @Log(title = "优惠券前台用户领取记录", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmStoreCouponIssueUserBo bo) {
        return toAjax(iWmStoreCouponIssueUserService.insertByBo(bo));
    }

    /**
     * 修改优惠券前台用户领取记录
     */
    @SaCheckPermission("coupon:storeCouponIssueUser:edit")
    @Log(title = "优惠券前台用户领取记录", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmStoreCouponIssueUserBo bo) {
        return toAjax(iWmStoreCouponIssueUserService.updateByBo(bo));
    }

    /**
     * 删除优惠券前台用户领取记录
     *
     * @param ids 主键串
     */
    @SaCheckPermission("coupon:storeCouponIssueUser:remove")
    @Log(title = "优惠券前台用户领取记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmStoreCouponIssueUserService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
