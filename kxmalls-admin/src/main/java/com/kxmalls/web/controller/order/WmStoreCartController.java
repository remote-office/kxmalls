package com.kxmalls.web.controller.order;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.order.domain.vo.WmStoreCartVo;
import com.kxmalls.order.domain.bo.WmStoreCartBo;
import com.kxmalls.order.service.IWmStoreCartService;
import com.kxmalls.common.core.page.TableDataInfo;

/**
 * 购物车
 *
 * @author kxmalls
 * @date 2023-02-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/order/storeCart")
public class WmStoreCartController extends BaseController {

    private final IWmStoreCartService iWmStoreCartService;

    /**
     * 查询购物车列表
     */
    @SaCheckPermission("order:storeCart:list")
    @GetMapping("/list")
    public TableDataInfo<WmStoreCartVo> list(WmStoreCartBo bo, PageQuery pageQuery) {
        return iWmStoreCartService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出购物车列表
     */
    @SaCheckPermission("order:storeCart:export")
    @Log(title = "购物车", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmStoreCartBo bo, HttpServletResponse response) {
        List<WmStoreCartVo> list = iWmStoreCartService.queryList(bo);
        ExcelUtil.exportExcel(list, "购物车", WmStoreCartVo.class, response);
    }

    /**
     * 获取购物车详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("order:storeCart:query")
    @GetMapping("/{id}")
    public R<WmStoreCartVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmStoreCartService.queryById(id));
    }

    /**
     * 新增购物车
     */
    @SaCheckPermission("order:storeCart:add")
    @Log(title = "购物车", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmStoreCartBo bo) {
        return toAjax(iWmStoreCartService.insertByBo(bo));
    }

    /**
     * 修改购物车
     */
    @SaCheckPermission("order:storeCart:edit")
    @Log(title = "购物车", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmStoreCartBo bo) {
        return toAjax(iWmStoreCartService.updateByBo(bo));
    }

    /**
     * 删除购物车
     *
     * @param ids 主键串
     */
    @SaCheckPermission("order:storeCart:remove")
    @Log(title = "购物车", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmStoreCartService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
