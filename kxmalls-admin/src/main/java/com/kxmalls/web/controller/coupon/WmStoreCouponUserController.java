package com.kxmalls.web.controller.coupon;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.page.TableDataInfo;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.coupon.domain.bo.WmStoreCouponUserBo;
import com.kxmalls.coupon.domain.vo.WmStoreCouponUserVo;
import com.kxmalls.coupon.service.IWmStoreCouponUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 优惠券发放记录
 *
 * @author kxmalls
 * @date 2023-02-17
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/coupon/storeCouponUser")
public class WmStoreCouponUserController extends BaseController {

    private final IWmStoreCouponUserService iWmStoreCouponUserService;

    /**
     * 查询优惠券发放记录列表
     */
    @SaCheckPermission("coupon:storeCouponUser:list")
    @GetMapping("/list")
    public TableDataInfo<WmStoreCouponUserVo> list(WmStoreCouponUserBo bo, PageQuery pageQuery) {
        return iWmStoreCouponUserService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出优惠券发放记录列表
     */
    @SaCheckPermission("coupon:storeCouponUser:export")
    @Log(title = "优惠券发放记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmStoreCouponUserBo bo, HttpServletResponse response) {
        List<WmStoreCouponUserVo> list = iWmStoreCouponUserService.queryList(bo);
        ExcelUtil.exportExcel(list, "优惠券发放记录", WmStoreCouponUserVo.class, response);
    }

    /**
     * 获取优惠券发放记录详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("coupon:storeCouponUser:query")
    @GetMapping("/{id}")
    public R<WmStoreCouponUserVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmStoreCouponUserService.queryById(id));
    }

    /**
     * 新增优惠券发放记录
     */
    @SaCheckPermission("coupon:storeCouponUser:add")
    @Log(title = "优惠券发放记录", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmStoreCouponUserBo bo) {
        return toAjax(iWmStoreCouponUserService.insertByBo(bo));
    }

    /**
     * 修改优惠券发放记录
     */
    @SaCheckPermission("coupon:storeCouponUser:edit")
    @Log(title = "优惠券发放记录", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmStoreCouponUserBo bo) {
        return toAjax(iWmStoreCouponUserService.updateByBo(bo));
    }

    /**
     * 删除优惠券发放记录
     *
     * @param ids 主键串
     */
    @SaCheckPermission("coupon:storeCouponUser:remove")
    @Log(title = "优惠券发放记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmStoreCouponUserService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
