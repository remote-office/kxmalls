package com.kxmalls.web.controller.product;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.kxmalls.common.annotation.RepeatSubmit;
import com.kxmalls.common.annotation.Log;
import com.kxmalls.common.core.controller.BaseController;
import com.kxmalls.common.core.domain.PageQuery;
import com.kxmalls.common.core.domain.R;
import com.kxmalls.common.core.validate.AddGroup;
import com.kxmalls.common.core.validate.EditGroup;
import com.kxmalls.common.enums.BusinessType;
import com.kxmalls.common.utils.poi.ExcelUtil;
import com.kxmalls.product.domain.vo.WmStoreProductAttrResultVo;
import com.kxmalls.product.domain.bo.WmStoreProductAttrResultBo;
import com.kxmalls.product.service.IWmStoreProductAttrResultService;
import com.kxmalls.common.core.page.TableDataInfo;

/**
 * 商品属性详情
 *
 * @author kxmalls
 * @date 2023-02-13
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/product/storeProductAttrResult")
public class WmStoreProductAttrResultController extends BaseController {

    private final IWmStoreProductAttrResultService iWmStoreProductAttrResultService;

    /**
     * 查询商品属性详情列表
     */
    @SaCheckPermission("product:storeProductAttrResult:list")
    @GetMapping("/list")
    public TableDataInfo<WmStoreProductAttrResultVo> list(WmStoreProductAttrResultBo bo, PageQuery pageQuery) {
        return iWmStoreProductAttrResultService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出商品属性详情列表
     */
    @SaCheckPermission("product:storeProductAttrResult:export")
    @Log(title = "商品属性详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WmStoreProductAttrResultBo bo, HttpServletResponse response) {
        List<WmStoreProductAttrResultVo> list = iWmStoreProductAttrResultService.queryList(bo);
        ExcelUtil.exportExcel(list, "商品属性详情", WmStoreProductAttrResultVo.class, response);
    }

    /**
     * 获取商品属性详情详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("product:storeProductAttrResult:query")
    @GetMapping("/{id}")
    public R<WmStoreProductAttrResultVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWmStoreProductAttrResultService.queryById(id));
    }

    /**
     * 新增商品属性详情
     */
    @SaCheckPermission("product:storeProductAttrResult:add")
    @Log(title = "商品属性详情", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WmStoreProductAttrResultBo bo) {
        return toAjax(iWmStoreProductAttrResultService.insertByBo(bo));
    }

    /**
     * 修改商品属性详情
     */
    @SaCheckPermission("product:storeProductAttrResult:edit")
    @Log(title = "商品属性详情", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WmStoreProductAttrResultBo bo) {
        return toAjax(iWmStoreProductAttrResultService.updateByBo(bo));
    }

    /**
     * 删除商品属性详情
     *
     * @param ids 主键串
     */
    @SaCheckPermission("product:storeProductAttrResult:remove")
    @Log(title = "商品属性详情", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWmStoreProductAttrResultService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
